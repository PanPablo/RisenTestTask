-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 01 Maj 2018, 00:40
-- Wersja serwera: 5.7.20
-- Wersja PHP: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `Risen`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_commentmeta`
--

CREATE TABLE `rs_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_comments`
--

CREATE TABLE `rs_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `rs_comments`
--

INSERT INTO `rs_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Komentator WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-04-30 11:51:27', '2018-04-30 09:51:27', 'Cześć, to jest komentarz.\nAby zapoznać się z moderowaniem, edycją i usuwaniem komentarzy, należy odwiedzić ekran Komentarze w kokpicie.\nAwatary komentujących pochodzą z <a href=\"https://pl.gravatar.com\">Gravatara</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_links`
--

CREATE TABLE `rs_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_options`
--

CREATE TABLE `rs_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `rs_options`
--

INSERT INTO `rs_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/RisenTestTask', 'yes'),
(2, 'home', 'http://localhost/RisenTestTask', 'yes'),
(3, 'blogname', 'Risen Task', 'yes'),
(4, 'blogdescription', 'Kolejna witryna oparta na WordPressie', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'pstruminski@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j F Y H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '', 'yes'),
(29, 'rewrite_rules', '', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:2:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:27:\"woocommerce/woocommerce.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'twentysixteen-child', 'yes'),
(41, 'stylesheet', 'twentysixteen-child', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'Europe/Warsaw', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'rs_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:114:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'WPLANG', 'pl_PL', 'yes'),
(95, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'sidebars_widgets', 'a:5:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(101, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'nonce_key', 'jL6?k6BR&2oEoZ3s.2;V!Ce(m6r12uim.=j>LPPvF,(~5_`s2F+&~w6WF/8t#cDA', 'no'),
(108, 'nonce_salt', 'E0HjR0%[t5g(P*|$RGMK2)u:9q.Q&>g*F3Ik1Cnni!8S%}S6R^,k<O1-,|:mU kf', 'no'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'cron', 'a:9:{i:1525136614;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1525167404;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1525168288;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1525168309;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1525171848;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1525210604;a:1:{s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1525212000;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1527768000;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:7:\"monthly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:2635200;}}}s:7:\"version\";i:2;}', 'yes'),
(113, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1525083793;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(123, 'auth_key', 'h:ES3SxR&$v2PFn@5B}jEm|h(emfo#Q^o3!u`>Q6{3u6a](b?5JsAcL5WyA7FK/o', 'no'),
(124, 'auth_salt', 'lW*k2#@FfJx,xTu%YPA@S?5*Wnmd|5dABZg+9e--nis=_(nfyNwylGL_*Y^hGJSx', 'no'),
(125, 'logged_in_key', 'Vm`rGd~Z6B9Ky[?-:aAHtiBf_|V2u@y<%}8s(fIJI[KFB$[?Dw6;gM$j|b4^w^;1', 'no'),
(126, 'logged_in_salt', '}MejkWu;2R{;N4i|Ow]*QmFHO;,c-bH~1:N,AZ8at1XwA1RAkXMDMpp^9oAUvO8p', 'no'),
(127, '_site_transient_timeout_browser_53ea26d59f5623b7f508c7d001b12c8d', '1525686709', 'no'),
(128, '_site_transient_browser_53ea26d59f5623b7f508c7d001b12c8d', 'a:10:{s:4:\"name\";s:6:\"Safari\";s:7:\"version\";s:4:\"11.1\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.apple.com/safari/\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/safari.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/safari.png?1\";s:15:\"current_version\";s:2:\"11\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(130, 'can_compress_scripts', '1', 'no'),
(140, '_transient_timeout_plugin_slugs', '1525213730', 'no'),
(141, '_transient_plugin_slugs', 'a:4:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:19:\"akismet/akismet.php\";i:2;s:9:\"hello.php\";i:3;s:27:\"woocommerce/woocommerce.php\";}', 'no'),
(142, 'recently_activated', 'a:2:{s:57:\"woocommerce-gateway-stripe/woocommerce-gateway-stripe.php\";i:1525124128;s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";i:1525124122;}', 'yes'),
(149, 'woocommerce_store_address', 'dsa', 'yes'),
(150, 'woocommerce_store_address_2', 'sda', 'yes'),
(151, 'woocommerce_store_city', 'sad', 'yes'),
(152, 'woocommerce_default_country', 'GB', 'yes'),
(153, 'woocommerce_store_postcode', 'sadas', 'yes'),
(154, 'woocommerce_allowed_countries', 'all', 'yes'),
(155, 'woocommerce_all_except_countries', '', 'yes'),
(156, 'woocommerce_specific_allowed_countries', '', 'yes'),
(157, 'woocommerce_ship_to_countries', '', 'yes'),
(158, 'woocommerce_specific_ship_to_countries', '', 'yes'),
(159, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(160, 'woocommerce_calc_taxes', 'no', 'yes'),
(161, 'woocommerce_currency', 'GBP', 'yes'),
(162, 'woocommerce_currency_pos', 'left', 'yes'),
(163, 'woocommerce_price_thousand_sep', ',', 'yes'),
(164, 'woocommerce_price_decimal_sep', '.', 'yes'),
(165, 'woocommerce_price_num_decimals', '2', 'yes'),
(166, 'woocommerce_shop_page_id', '2', 'yes'),
(167, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(168, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(169, 'woocommerce_weight_unit', 'kg', 'yes'),
(170, 'woocommerce_dimension_unit', 'cm', 'yes'),
(171, 'woocommerce_enable_reviews', 'yes', 'yes'),
(172, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(173, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(174, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(175, 'woocommerce_review_rating_required', 'yes', 'no'),
(176, 'woocommerce_manage_stock', 'yes', 'yes'),
(177, 'woocommerce_hold_stock_minutes', '60', 'no'),
(178, 'woocommerce_notify_low_stock', 'yes', 'no'),
(179, 'woocommerce_notify_no_stock', 'yes', 'no'),
(180, 'woocommerce_stock_email_recipient', 'pstruminski@gmail.com', 'no'),
(181, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(182, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(183, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(184, 'woocommerce_stock_format', '', 'yes'),
(185, 'woocommerce_file_download_method', 'force', 'no'),
(186, 'woocommerce_downloads_require_login', 'no', 'no'),
(187, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(188, 'woocommerce_prices_include_tax', 'no', 'yes'),
(189, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(190, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(191, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(192, 'woocommerce_tax_classes', 'Reduced rate\nZero rate', 'yes'),
(193, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(194, 'woocommerce_tax_display_cart', 'excl', 'no'),
(195, 'woocommerce_price_display_suffix', '', 'yes'),
(196, 'woocommerce_tax_total_display', 'itemized', 'no'),
(197, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(198, 'woocommerce_shipping_cost_requires_address', 'no', 'no'),
(199, 'woocommerce_ship_to_destination', 'billing', 'no'),
(200, 'woocommerce_shipping_debug_mode', 'no', 'no'),
(201, 'woocommerce_enable_coupons', 'yes', 'yes'),
(202, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(203, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(204, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(205, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(206, 'woocommerce_cart_page_id', '27', 'yes'),
(207, 'woocommerce_checkout_page_id', '28', 'yes'),
(208, 'woocommerce_terms_page_id', '', 'no'),
(209, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(210, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(211, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(212, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(213, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(214, 'woocommerce_myaccount_page_id', '29', 'yes'),
(215, 'woocommerce_enable_signup_and_login_from_checkout', 'yes', 'no'),
(216, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(217, 'woocommerce_enable_checkout_login_reminder', 'yes', 'no'),
(218, 'woocommerce_registration_generate_username', 'yes', 'no'),
(219, 'woocommerce_registration_generate_password', 'no', 'no'),
(220, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(221, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(222, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(223, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(224, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(225, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(226, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(227, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(228, 'woocommerce_email_from_name', 'Risen Task', 'no'),
(229, 'woocommerce_email_from_address', 'pstruminski@gmail.com', 'no'),
(230, 'woocommerce_email_header_image', '', 'no'),
(231, 'woocommerce_email_footer_text', '{site_title}', 'no'),
(232, 'woocommerce_email_base_color', '#96588a', 'no'),
(233, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(234, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(235, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(236, 'woocommerce_api_enabled', 'yes', 'yes'),
(237, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:7:\"product\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(238, 'current_theme_supports_woocommerce', 'no', 'yes'),
(239, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(240, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(241, 'product_cat_children', 'a:0:{}', 'yes'),
(242, 'default_product_cat', '15', 'yes'),
(247, 'woocommerce_admin_notices', 'a:0:{}', 'yes'),
(248, '_transient_woocommerce_webhook_ids', 'a:0:{}', 'yes'),
(249, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(250, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(251, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(252, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(253, 'widget_woocommerce_product_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(254, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(255, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(256, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(257, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(258, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(259, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(260, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(262, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:8:\"approved\";s:1:\"1\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(263, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(264, '_transient_timeout_wc_report_sales_by_date', '1525221378', 'no'),
(265, '_transient_wc_report_sales_by_date', 'a:464:{s:32:\"dab4085593771f943a5bcc4c12055c4a\";a:0:{}s:32:\"ba461d3627eda936c510dd1cea1fe85e\";a:0:{}s:32:\"156ed3f20b6f5becf23bcdaa9beeb86a\";a:0:{}s:32:\"bf565f3b76d0922ddb7788ac7dd65bbd\";N;s:32:\"6a36ef09d92b44d617f4fab915b17cb5\";a:0:{}s:32:\"031fb7b0a58c54c8cb874ef6109d94a8\";a:0:{}s:32:\"cd24f4184fd72c0e69ca19514525bde1\";a:0:{}s:32:\"beda7a7b2029829b64a9bc5abba864e6\";a:0:{}s:32:\"dfd0eafeb6641bc1bc52aea8511784cc\";a:0:{}s:32:\"08f42dc4e894a963aa2f334995507f44\";a:0:{}s:32:\"2d523d40750f158fe56a2568632c0013\";a:0:{}s:32:\"f2fc6b6ae0c15fe098acb9b30c86f9a5\";N;s:32:\"eb40600c3c8f968555b9d2b92eac98fb\";a:0:{}s:32:\"202ac41b589e85567715d0fe1ec9f7b8\";a:0:{}s:32:\"f34512c173219d2065c9aa68d0c6d2fb\";a:0:{}s:32:\"496d010064c28e2eb93c2ae8ee7e6d14\";a:0:{}s:32:\"56731b8177e1e4918af1eed0c2eeed96\";a:0:{}s:32:\"9b843adc3bb9d04925b5510b07ab498b\";a:0:{}s:32:\"e56bce8b8e2411a09171081c21603c12\";a:0:{}s:32:\"6d28c3138a0896ae7bf5288f8da9ec3e\";N;s:32:\"1f0e1325a9a68304020fbb9df8354eba\";a:0:{}s:32:\"677c466621ebae82eadaaf2995a3223b\";a:0:{}s:32:\"c24275a359bb30b9f0910ead565021bd\";a:0:{}s:32:\"2cecdb39b8f2dbfc7c13127463463885\";a:0:{}s:32:\"6a4f7343ae31ca8da9fe2c89fbc1950e\";a:0:{}s:32:\"ccbea887cb161b4ff83393a16f4c7499\";a:0:{}s:32:\"6dc3bd5e6717a72360a393123e93769b\";a:0:{}s:32:\"2efde7678acb661e9ca2bb26aa5631b3\";N;s:32:\"d8bf7411a374070245bce25a67b4af8e\";a:0:{}s:32:\"66a0fbe9ade008e5e702f241ce5f209a\";a:0:{}s:32:\"e63fdcd4b3d02a46b847e8aea26daca7\";a:0:{}s:32:\"f50bd2be8adae6d26f3aa4511beb9c42\";a:0:{}s:32:\"b5bfc1e17c825c8cbdeeccc7ca42921f\";a:0:{}s:32:\"df5f23495c0d8dbb8ef9db219e2736f6\";a:0:{}s:32:\"cae7dc6cb6de52b01a5ae6d8c44b13ab\";a:0:{}s:32:\"d404116a834c8e6855a8d2bb4d7db316\";N;s:32:\"90088cef78fa6b71a8732d2cfbfaf925\";a:0:{}s:32:\"782f10d49c81fe42f35ce98f3cba4a97\";a:0:{}s:32:\"c1d8c26746a528fc1fe4ebb49d403162\";a:0:{}s:32:\"dec051bddbd1ebb2bfe9389500cf9547\";a:0:{}s:32:\"9414f3f902e3ed503146971bc5613dcd\";a:0:{}s:32:\"1750981cb1bcffcd4febbc0a5c3d832e\";a:0:{}s:32:\"ba46b22b393acfdd4b81c25381089cd2\";a:0:{}s:32:\"7e0b6fa776d81234f1cab1597d91ae32\";N;s:32:\"6b94e6af0b008c15b9b84f93053c2664\";a:0:{}s:32:\"03ae50b2f0f3ee6604bab595a1cfe20a\";a:0:{}s:32:\"1b277b47182945145d3f06f5f30e7369\";a:0:{}s:32:\"feae893765532f2393d08447996af5ad\";a:0:{}s:32:\"159526bbf01ddebbc33276b46d2a7bba\";a:0:{}s:32:\"ee45f3930688ccdd3fa7a521b1315157\";a:0:{}s:32:\"fb58f541154843383ec1c5ded429da28\";a:0:{}s:32:\"fb09538f70a55e643bcfdcffe80cf20d\";N;s:32:\"546fe800b130d3aac90a2bbbdcd2392d\";a:0:{}s:32:\"da66f7bdd6fc0728abf66ebaa314fadc\";a:0:{}s:32:\"4714dc5dc99ecb861a2e93d9cb606b51\";a:0:{}s:32:\"9b380f2418aee6605685aa7dfe32cae6\";a:0:{}s:32:\"9ad7e4b99f9a06bc0d605a49904db0df\";a:0:{}s:32:\"cb65af0428ad7e096be6cae774cdb14e\";a:0:{}s:32:\"f54c14b3b423586f226fb6c59a766581\";a:0:{}s:32:\"43b38e33346e0e1ec21492be0f68a688\";N;s:32:\"3311371a6e9f89775ec612263459ecd2\";a:0:{}s:32:\"f4f9c4fe01e1d6f57a318ec93577d7e0\";a:0:{}s:32:\"f3556972e5cedd31b536444783228e99\";a:0:{}s:32:\"3e28c9e4a54b96041cb52d6b546c293b\";a:0:{}s:32:\"245d1930ba48e57d84ed66d14140eb9a\";a:0:{}s:32:\"5b5478baa09244c564add90850705cf2\";a:0:{}s:32:\"11ca5741658f0f0dd9becad6ebf04e46\";a:0:{}s:32:\"4b92c54c9614f27c20bc097734feed6c\";N;s:32:\"739d9cac524a62d6ca35542a5a0b22c1\";a:0:{}s:32:\"d38a79665e09ee8d9a12d8d4abfb2354\";a:0:{}s:32:\"960421e33e083fbfa071b7bd1508310f\";a:0:{}s:32:\"fbaa64f8cf603e6e50d4509f65ee2c04\";a:0:{}s:32:\"5bc1559325a57e3da1fd3758a40a71b1\";a:0:{}s:32:\"c5544dc6a84ef9e0fc7d52a063f2e413\";a:0:{}s:32:\"0ec11f486fa556cefb3c72122955fc24\";a:0:{}s:32:\"728f9a13aa2dea69859ef19a619aad36\";N;s:32:\"03064d344e58cc4100ad3a3ac577c2c7\";a:0:{}s:32:\"9be53c3ec152c1d2c5bb8d9342161537\";a:0:{}s:32:\"b8b3bccef4bfd80622e5d7761feb88ac\";a:0:{}s:32:\"e80ba1305a21e6caed0fbfe40e561c53\";a:0:{}s:32:\"874eeb9a2b8e0dbde3aec1a4e0c73669\";a:0:{}s:32:\"9b8851b6e98d2d784676aff0d79ae6ef\";a:0:{}s:32:\"efcca6236b83a7ea0628ee51f6e87fa7\";a:0:{}s:32:\"97f030038cd349c06a3440bc69dbae8f\";N;s:32:\"ac977f5bf6bda268a8383dd043c9cadc\";a:0:{}s:32:\"d47fdc3592b0ce12085bb773c7adfb50\";a:0:{}s:32:\"558ff4db49a286de47aca03625ff0639\";a:0:{}s:32:\"e117ac0246770780b4a8b3314aeea91e\";a:0:{}s:32:\"a27d726f877b1e24956988306763fa80\";a:0:{}s:32:\"6b000549239641bca4eb0567180c3bb9\";a:0:{}s:32:\"5b6b256adffe41e9b89383145d8889c9\";a:0:{}s:32:\"04850567aca24ad88336a35e3a612ccb\";N;s:32:\"4f5afadea00468028d858e8c58dee602\";a:0:{}s:32:\"49f65a29f107f98b8af534b1284e0b10\";a:0:{}s:32:\"00dd0b51db0488dcd88db4c6f8b9ffe0\";a:0:{}s:32:\"59af6ee06f7f4e07c1d5d7681beda318\";a:0:{}s:32:\"e67badbd39d7bbd34ee754e6305dc472\";a:0:{}s:32:\"46d24fa0a0a83a14d0a9e8d007c3b4f7\";a:0:{}s:32:\"63f8d6129031a5dd8b6dd01d1f8f9c1e\";a:0:{}s:32:\"29942057731fcb72767c15af9dc54cbd\";N;s:32:\"eaf75892de0390011f26c7ed81835796\";a:0:{}s:32:\"48c52a3451678e0a480683d0abe49aab\";a:0:{}s:32:\"d95bdacf809f244910711000b48286e6\";a:0:{}s:32:\"54221391a8f5a5949d023b3063b84789\";a:0:{}s:32:\"9a3ad0e3014b4e77bc103e23173f2076\";a:0:{}s:32:\"ab4c3145352f7735bb92f4b083593f9f\";a:0:{}s:32:\"91e7ef0d7db90d04cdd37f530d3e9bad\";a:0:{}s:32:\"7478d4c4fe766308f8a56bd683142f72\";N;s:32:\"1b8044f3aac990be93f28cd8989fbcf2\";a:0:{}s:32:\"6a5245cb36dfe4ca306a43715fb2c78b\";a:0:{}s:32:\"c54d21091a6f630ea7b17b9ec7e8cb27\";a:0:{}s:32:\"626a1a7ac7c6ac7dfcead74ea687ef20\";a:0:{}s:32:\"2f235958297b56c8a96fcb1c0ceb72b9\";a:0:{}s:32:\"f182f9e390aca7642346d818474a59e4\";a:0:{}s:32:\"35193dba87c07267d419ba8eeddbd6f2\";a:0:{}s:32:\"ed2d580f4f6171c74e4c4f811bae1e09\";N;s:32:\"d144a46ad8655d8e7efe985da3e18deb\";a:0:{}s:32:\"dc04ae5136fd7c242f7b1f5ae6014032\";a:0:{}s:32:\"fb49b3d710d4c01c33271357bf1bf4dc\";a:0:{}s:32:\"46306258dda26f7797355eddf0c5fab5\";a:0:{}s:32:\"25f3738d7b9313b3bc198c188acbde44\";a:0:{}s:32:\"76cc416ed9608e44c000b931f23db83c\";a:0:{}s:32:\"cc1c4e8aa8de9006949a9e1bac50c79a\";a:0:{}s:32:\"a613f1b711958526f3e5a238c3b64625\";N;s:32:\"e88631abd88d239395db8e11b7cbf720\";a:0:{}s:32:\"1908342cf5237b535aecec18474ae94b\";a:0:{}s:32:\"f458e4e1e57e0bee0c56f787e959e6df\";a:0:{}s:32:\"8c1107c1aef076578065d036e3292fa8\";a:0:{}s:32:\"3079b9fecc3159c841c2c1b9a8d04da9\";a:0:{}s:32:\"073523cb390fe7ceee4e5051aaa06bbf\";a:0:{}s:32:\"bd756dc039a6c4f372ef4e07b831c034\";a:0:{}s:32:\"c3f62fef2b317cb6ff7926a35ae47e26\";N;s:32:\"51fcb5078b9bf39639c9b8f32e776269\";a:0:{}s:32:\"52f5f1768dc97125230e1ada9fa8f7f5\";a:0:{}s:32:\"d14b7214c535212727fc3191428d4810\";a:0:{}s:32:\"5ddbecf1b5e50b19c86fbf69d021d21e\";a:0:{}s:32:\"e46e98e4914e0ee789b90c18fc56e16b\";a:0:{}s:32:\"f724a54fc4940c5c1f90cf705ac0a69e\";a:0:{}s:32:\"69c30e8de7f162d275260e9fec620dbf\";a:0:{}s:32:\"ac35ea4b88ba3269d38801ea3ab1f218\";N;s:32:\"c495cf1d580bd3c2d9dbf86e01142b29\";a:0:{}s:32:\"96913a45995761b250762b91c8061ab7\";a:0:{}s:32:\"f581f9b226b861ba9800b24a74fdf9e6\";a:0:{}s:32:\"2d26946f1545e81e2cc7e932b1f8200a\";a:0:{}s:32:\"6e09a155d43b038a7cd6eabe495f87ab\";a:0:{}s:32:\"bd90313612264b5a727eecfc1b5146a4\";a:0:{}s:32:\"67f871052dfaf3dd97dc878c66a8b5a5\";a:0:{}s:32:\"264589bc0de77cfe5ce3ba0cc52961dc\";N;s:32:\"872f8f8124ca7699e5152ad026b05866\";a:0:{}s:32:\"cd34ac1d2c8906e6b1bdc0bc3245e5b7\";a:0:{}s:32:\"8324ab712a80af57f2960e54969b38a6\";a:0:{}s:32:\"9d1cb68a34d68552ffd843036ab7271a\";a:0:{}s:32:\"8d679d790387ab42aec2e47f969561a1\";a:0:{}s:32:\"3ef0fa443702d7cc16fd111aba482d24\";a:0:{}s:32:\"3805eb8e6e453cc51eef0b864fdd22d1\";a:0:{}s:32:\"c1e8f73d30d42c1e604531ee76781a9b\";N;s:32:\"d88143f45080142d8cc45d40e3bc27fb\";a:0:{}s:32:\"d57636630234d883ef3277b05a89015b\";a:0:{}s:32:\"b7f4f5749005b1c1f1225d1534dc45a4\";a:0:{}s:32:\"3c1cb709b0f77597e2c5430dbd15d4fd\";a:0:{}s:32:\"cdc6a976696ea93ff7630d2ee91bc1fc\";a:0:{}s:32:\"3cfbcd9eca6158ae21b96fd76b7ca918\";a:0:{}s:32:\"1ded9c2d5b5f8943e02ecb9f6ac01a2e\";a:0:{}s:32:\"98f3d0386258ea9810f86f800ea56d69\";N;s:32:\"b4add0d59f4a700e4d6bb58903749072\";a:0:{}s:32:\"decbe56d969dcbaf07a896f6a1e09153\";a:0:{}s:32:\"b04590521248ed3df5ab6c7046238723\";a:0:{}s:32:\"38f80d5b3c037cca3a941f711f022788\";a:0:{}s:32:\"d27ee7d7a4efa472db2c73205b254fd4\";a:0:{}s:32:\"dd3f020dac1ad0b45c0da32078ffa9e7\";a:0:{}s:32:\"c4bdbf42db2c4195ab97de709d7d19eb\";a:0:{}s:32:\"ce52d67da8fc4183d7a662fcf7ced618\";N;s:32:\"1b668801207676e6d1959ca97946e0cf\";a:0:{}s:32:\"8d0989d5444f15d8c4e31a36e715708b\";a:0:{}s:32:\"fa3bce35b80e66bc04c944fb6d757bcf\";a:0:{}s:32:\"5528d5c4b6bf17ef84b9dc0bc9eb99b1\";a:0:{}s:32:\"471e2887f533599d90f34ff25820dce2\";a:0:{}s:32:\"6f5503663398154b9eee8661f80c9331\";a:0:{}s:32:\"c67cc5e696cf5a85018607fba0d8b336\";a:0:{}s:32:\"97b5f20af4aff022b6ddcebe5dd1a124\";N;s:32:\"2a9e3197136bc4f80368e98224b29396\";a:0:{}s:32:\"a5d09df2ea04351688d0bb14b0b988ce\";a:0:{}s:32:\"be2909d1d2882933814ab7e94073f059\";a:0:{}s:32:\"e331136a35f0174f3b75d36b594555d0\";a:0:{}s:32:\"0cad8fbf22017649367e29d93cb9e6ed\";a:0:{}s:32:\"b03cefa29891d73fa3045fe67d72ba95\";a:0:{}s:32:\"bf00b81c682d608746ad9d456e5eed70\";a:0:{}s:32:\"fc4da37c22f0015f9c5c1505c8dad660\";N;s:32:\"19ab1e05e27abd748ece4691853d4e80\";a:0:{}s:32:\"8e737fcff9f0c793f7d4d0592c8b07da\";a:0:{}s:32:\"6c743622626a15fa45ce24e764f4f397\";a:0:{}s:32:\"24e0306c0f615b1d7995cad0b889ea79\";a:0:{}s:32:\"597375373d0f6657a0e73fcdfa75844f\";a:0:{}s:32:\"19b8da55424ae785550f5320bb4621b9\";a:0:{}s:32:\"54ff4801f71dacac166ce1d4c1a27c6a\";a:0:{}s:32:\"bd0a1dd07011a07d8f5edc26fe01ad94\";N;s:32:\"094e4b96dae43a87834c7d44fb602102\";a:0:{}s:32:\"2a6c60309574f3ac8e2acbc0498178a7\";a:0:{}s:32:\"08cb9fdbecbaa13cdeb8275aca32c387\";a:0:{}s:32:\"cce4a982d0cd1c3aea2d0de81f9e65ca\";a:0:{}s:32:\"54fee0f1a359c76ba551ad60b487eee9\";a:0:{}s:32:\"5f3f72d97ed5f059d1b838c87548f737\";a:0:{}s:32:\"0c547d4576375203cb1ae9dc9eea7984\";a:0:{}s:32:\"6a27830d28d38d5fc07b057a72ba9277\";N;s:32:\"5af2e31fb025e40595fe6cf1e362df1f\";a:0:{}s:32:\"384aded315d59ad0ed2254b4f64bd204\";a:0:{}s:32:\"56de2733628a287ae8c27ac30173d17d\";a:0:{}s:32:\"77fba02f4362d45bfcf564f0434e6098\";a:0:{}s:32:\"00c21093813a04094bd105fa9267ccc0\";a:0:{}s:32:\"09a4ad71f7d190e6c2604d765b6d44c5\";a:0:{}s:32:\"b3f4b71472b65cdff1fa82ec35d701f7\";a:0:{}s:32:\"1c1f5da1212ace78b481104eb03d5987\";N;s:32:\"ea6dbef1668a1d1af74aed9975a75c4a\";a:0:{}s:32:\"0932975ce2e79cdf3edcd9b6909d782a\";a:0:{}s:32:\"96ad3a25e8190587c91aca4e4e3e2982\";a:0:{}s:32:\"dbd96429eefcc77b5e84085efcea7e56\";a:0:{}s:32:\"d311422dd628aca52071670baf7b9110\";a:0:{}s:32:\"8845ec263bb5af78da9e66c7ad29d663\";a:0:{}s:32:\"68c3eb1be7a81309cdc86bdd696de8c6\";a:0:{}s:32:\"5f6fc0e9d455e8e10c4045e89ad98e09\";N;s:32:\"f64e83c1a0042623a81a85a0f25cb715\";a:0:{}s:32:\"54cecddaa93af34a32a6461d5e07bf71\";a:0:{}s:32:\"c1cfb3f61a1634fd1abe032299948dc2\";a:0:{}s:32:\"fa7d22bf08177354a0db069da5f62d04\";a:0:{}s:32:\"b172d442104a56f4c0f7ff21433c42df\";a:0:{}s:32:\"b1bb3e5ee9fda8171b438dca2e1c2dc6\";a:0:{}s:32:\"01e2530c79ed5da5b7a40a4b82dc3c38\";a:0:{}s:32:\"3bd84ca606f92caa019b38a56863139f\";N;s:32:\"c5f76a6bd82997fd0d384aa836655f83\";a:0:{}s:32:\"c6f1118724be1eac1b702bd09d1034e0\";a:0:{}s:32:\"bf125344f22581472d5a5797be9aed87\";a:0:{}s:32:\"93092e6d95f0bd07a94f9d19ee2115d6\";a:0:{}s:32:\"518c5faf7854646bb5ed9da2fca9b73f\";a:0:{}s:32:\"f774bc3a3ed7e4dc1f70398cb60a543f\";a:0:{}s:32:\"02f08b49b93296ecb4f31fe4822ad1ea\";a:0:{}s:32:\"e747802bab5d8d80bc0e2083cd3bb231\";N;s:32:\"d411474df4117bb481704abfd278d67a\";a:0:{}s:32:\"2ae0829b83d390324efac62d1b480229\";a:0:{}s:32:\"9a5746fc22265f7aea926e237b1b34e2\";a:0:{}s:32:\"a3d42cfd60dc0c0f58e9674b4b672b26\";a:0:{}s:32:\"a66aac0e170b24d1f1ca7d82db647738\";a:0:{}s:32:\"46413eb46b517b357505961f7a5674a2\";a:0:{}s:32:\"3778137454750e7e3003261219ebbd37\";a:0:{}s:32:\"c25bb112a78a1227e7a9a72dd1f6a19e\";N;s:32:\"17e281acffac236e6f8ee8d0d255a229\";a:0:{}s:32:\"5cf442e495b9cc3fe4cb86307be6f945\";a:0:{}s:32:\"1a8d69b4482281a8ef17c1ec4f50165a\";a:0:{}s:32:\"f5deacd4827a5311895355bc31d5ea69\";a:0:{}s:32:\"bfed9974955a73f4c9b99ff46e2fbf9a\";a:0:{}s:32:\"877ee4a8d035091d4998690179ad1662\";a:0:{}s:32:\"47b3b2f4058736c0ad7742a0d4ef199a\";a:0:{}s:32:\"b5021c9f07637b2e6fc9b7dd31a47f0b\";N;s:32:\"5fe47213cc5097ac0eabff625dda28aa\";a:0:{}s:32:\"8639fb8f9c6607aa15777213f86ec620\";a:0:{}s:32:\"dcc63dffcb65d0b6081d5ccbd4c39fdb\";a:0:{}s:32:\"780a1ddb9f5ca215eb9dcaf3f4edccd2\";a:0:{}s:32:\"d6d7cda01271c1f2108c4990f6116726\";a:0:{}s:32:\"ae8a6f8afab7c3456e575851174449bb\";a:0:{}s:32:\"b065d4f95647c6e8919d16333764a03d\";a:0:{}s:32:\"b3f081299ddabc9f2a14be2dd3fa1e3e\";N;s:32:\"7ccfb8a6575a67361f8c6d114c0a6b48\";a:0:{}s:32:\"fe93d851657f15543bfa795cc96020ca\";a:0:{}s:32:\"4abe55e382feffaafec1532d062c6e53\";a:0:{}s:32:\"0ecb4360ae6f62cdfd3df19828ae48e2\";a:0:{}s:32:\"743875bedc8c14023c7a7f7d4243bfc3\";a:0:{}s:32:\"5bb2f00fc0230c6cd77f33bd60ec7b16\";a:0:{}s:32:\"265abbaf2e1a084cb3bebc34ff4a0c0f\";a:0:{}s:32:\"947d9da21256f2371119010c8c48005d\";N;s:32:\"5a3b09531efb2464464b39ab00f59e60\";a:0:{}s:32:\"bcfd4033e3d1a9990bb098efb4cde5ae\";a:0:{}s:32:\"1e47a975935207b242c15cf3e4d93127\";a:0:{}s:32:\"197a81c8fdbb56c91c854004b76ed61d\";a:0:{}s:32:\"be977d7d9c7f5e0287dbacfa8bb4d33a\";a:0:{}s:32:\"48b1ff6f5e26c5e6e22e8cca586c9b1b\";a:0:{}s:32:\"8fdf118e94bdf6be3003bd2b8ba09cd5\";a:0:{}s:32:\"04cb77fcd559fe37b8f3fb35cd03d53c\";N;s:32:\"62343b21a47779e440ed73846d66bccc\";a:0:{}s:32:\"af1411edec22a39f13f6edb41e521c41\";a:0:{}s:32:\"59ec33e9bc1e28312b4744c6fba4b3b6\";a:0:{}s:32:\"bec51a78002a6fb7beeac8a74032b259\";a:0:{}s:32:\"2987e3e7123f393e615aa974ec131338\";a:0:{}s:32:\"fed552e4362f78a498bea48f638ce354\";a:0:{}s:32:\"0574aef9dfba748e0ca22517b3cfc70a\";a:0:{}s:32:\"732403b72287b6f617db32c7b50bec79\";N;s:32:\"a30be7f5451ba23776638dcded43baa2\";a:0:{}s:32:\"ec4be2ea221bde844ede008d1a60d31a\";a:0:{}s:32:\"78167e5587d767d0120df0880fc776fe\";a:0:{}s:32:\"7e74c2c918d6900af58fa668e3401aa0\";a:0:{}s:32:\"62bdd46b30d1d4743afb98d6166205b8\";a:0:{}s:32:\"606e21e5a77a139e461223332d8ecb9b\";a:0:{}s:32:\"264fff3ed5734a739ef4e26d291e9e36\";a:0:{}s:32:\"74d1fe67bf96c65eaf66a9cf6cb8f82c\";N;s:32:\"06ba55bf2ba8c1b0c2f55f4cb35f8982\";a:0:{}s:32:\"a23f577e0935a03442a5e78c2bd422dc\";a:0:{}s:32:\"95674685ccfdade26f12e8b59782a155\";a:0:{}s:32:\"4de7750937d6524ffb30d1cb45794dde\";a:0:{}s:32:\"00d3938cd1de72af713eb59e9731417d\";a:0:{}s:32:\"fa10bfbb05d0fc3a70e6800efb5b7c28\";a:0:{}s:32:\"ad1f9e143eff619d79393efee415e6e8\";a:0:{}s:32:\"470b91c5b61d39278e896fb064712a53\";N;s:32:\"f86e3683b6b2f1484534c98ff98b2713\";a:0:{}s:32:\"4c8e0198229ee1e0bd67ef686eb4e91f\";a:0:{}s:32:\"3af7477cc2bcc0a2953bd43a7fdb2c70\";a:0:{}s:32:\"df373e36fa85a2b361653ca587a1e1c7\";a:0:{}s:32:\"17e4b2cd19aec80b2bfc24602be19a7c\";a:0:{}s:32:\"2095817ceaec4a9d260e1cceb4e1d9ce\";a:0:{}s:32:\"0994d1b212ec4662d292b9653b0b5779\";a:0:{}s:32:\"a8cc11f67c5d410152bd1fa6de9ccdc7\";N;s:32:\"313a5cff9b14b64c56669b1d6cd1e802\";a:0:{}s:32:\"454bab6c3e89dd1046635459e240c44e\";a:0:{}s:32:\"ad3cbad3d485c347536ea0e5b7f9d5fe\";a:0:{}s:32:\"01dc3206494324e683d2592d4a87f48d\";a:0:{}s:32:\"4fa6bd70fad12dc35305bd30d9807bd3\";a:0:{}s:32:\"cb0eb9583baec8f692f1db29e63fa68e\";a:0:{}s:32:\"18742920793efe637a262185d0633b4a\";a:0:{}s:32:\"efc48f740b7aceb7fb2cd90f6da52e99\";N;s:32:\"e95283f17a750e1aefde2dab46c29c3b\";a:0:{}s:32:\"0fa95f582e79dca9f4e9c5e3e7013c4b\";a:0:{}s:32:\"d1840636d7dce059aeff78e1bd27c974\";a:0:{}s:32:\"e0c8515e77f16b886e4fa5f9267a7f3e\";a:0:{}s:32:\"2cfb4dddaa563f2a2512192df348eaff\";a:0:{}s:32:\"3d679ea4c0eba01a5a26ad68dbfda0fa\";a:0:{}s:32:\"351fa346d09d608afa3b88b46770f0b1\";a:0:{}s:32:\"8dcd64b8bcfcf8b6fbe4fe83e3787342\";N;s:32:\"db838f69efab258b894ba2bbe54d28c6\";a:0:{}s:32:\"45b9cd17958517a03b1202e14d397c57\";a:0:{}s:32:\"1367b782531fcde2b8af1c73bea3c2ed\";a:0:{}s:32:\"5b713e42e8c73cda624e79d85b018e72\";a:0:{}s:32:\"7c21b44631920ad2c26c6e8b7dd76be8\";a:0:{}s:32:\"ac24f71a439784b6a2bcef3b2c13e95f\";a:0:{}s:32:\"cdb4c1c84fc0e905c95f67e3334504c8\";a:0:{}s:32:\"af5f9471504c3496351dfc4b385e5f52\";N;s:32:\"a689300d04ba373115c81d1661276c61\";a:0:{}s:32:\"cedd093b03958fe291d596dc20284cb5\";a:0:{}s:32:\"7e05cbda9dc8855fba931b36b6135dd9\";a:0:{}s:32:\"dbf3b12ab2d132c06bef1f458d93490f\";a:0:{}s:32:\"1083f0299d749f06aa615f95ea726d7f\";a:0:{}s:32:\"8748f7059d1b1b113949d22b3bcac7ed\";a:0:{}s:32:\"c17eca38ad51a75e0980a84726f888a2\";a:0:{}s:32:\"39cda5579166ece425b39998016f9b93\";N;s:32:\"5fa72cef9d95734bc33251cee30bde6b\";a:0:{}s:32:\"c30ac29527e559caca1bb2f74c02011c\";a:0:{}s:32:\"09d41032e45474bfcb89cf5968e00c3e\";a:0:{}s:32:\"b982b8d6519a00f60da08d1084e004dc\";a:0:{}s:32:\"377c369c0caf11cabcdbcd921c647479\";a:0:{}s:32:\"e51adc19efe4e10f66fd2eb3447a872d\";a:0:{}s:32:\"17e8277dcbdfdb0e4330b0d519096bf3\";a:0:{}s:32:\"eaa9f0d94f0843fcc7167abe6fa5dec0\";N;s:32:\"a5f0156ea3ded011a8488f82a5d93f4f\";a:0:{}s:32:\"7d5172c476aa8862b28e74a1f024fdc1\";a:0:{}s:32:\"a3d3df04a4655b9090b27df0ba503c34\";a:0:{}s:32:\"34ae66288c4b0cd600ee9f4ae0da81ae\";a:0:{}s:32:\"ad0c60ba3e06f0821080d8a0a485ef72\";a:0:{}s:32:\"af43cc454dd3be8fc8d12b5643e49898\";a:0:{}s:32:\"bcd569c381370451fbcec0c668b4d592\";a:0:{}s:32:\"43d5b2e410d550377ecdcf416b3fd2d6\";N;s:32:\"d9e802a2bcaf61f83d772e87be7b0b04\";a:0:{}s:32:\"a8ba8d8945663e7869877552d6e52e39\";a:0:{}s:32:\"ac8e50294ed778b9d6c8ae2e9a358d9b\";a:0:{}s:32:\"c8cc8f893b6b0c5c310c35408cb71075\";a:0:{}s:32:\"7354212229ccd6369e2ce8a672b69b82\";a:0:{}s:32:\"791e24b31c2ff73a9a4adcdfb6d70b70\";a:0:{}s:32:\"841cd696835cb60c7dbe1652c2debd80\";a:0:{}s:32:\"45e09c0cecccd4aefd1d15c55e60155e\";N;s:32:\"9d9e639bf2d21a566b39656bac86a563\";a:0:{}s:32:\"cc53fe466053d559e1c17ce96cdbeade\";a:0:{}s:32:\"25a6490a53c54473576837ff76646104\";a:0:{}s:32:\"a090f5982770284d418ef80878af5bd2\";a:0:{}s:32:\"98394efa61903d5d44e54b75fab3192b\";a:0:{}s:32:\"a5819fc0e93241dd055f9b9e474b05a6\";a:0:{}s:32:\"08c1593588e7d891d6d5001703f71d63\";a:0:{}s:32:\"fcb875b82232e55f3745f251b812575b\";N;s:32:\"e426ac434931b2e59bf4c677eab83815\";a:0:{}s:32:\"0064c9dfe55efdb4e79b45ebd3150db2\";a:0:{}s:32:\"80f6074d7e6fd644b5fe62143e16cca4\";a:0:{}s:32:\"36c7726177d75ef2edee549861f67707\";a:0:{}s:32:\"b3803fb39329b98f82730673bf783d78\";a:0:{}s:32:\"4727d5f503e1e180d47d4c6cdda00f32\";a:0:{}s:32:\"55335114c4efac670074d6057b7729c2\";a:0:{}s:32:\"f7cc8731633533efa87d32dbbc89e450\";N;s:32:\"7b2be8e36048ca3f93fb6d3969dde4f5\";a:0:{}s:32:\"ecb57439720e156a28925e0f38fb92b0\";a:0:{}s:32:\"0a658eaf08f960560312b39537a1a550\";a:0:{}s:32:\"6cb6aa0f2f158710ec96ccc594b266da\";a:0:{}s:32:\"053e195e957fe74ff28d0ae8266ad1f8\";a:0:{}s:32:\"ff72080d634609a1c38aac7073ad8873\";a:0:{}s:32:\"aa7a163ca8dfd09591232085991ff514\";a:0:{}s:32:\"04ff7897e91cbfee870d008870f797b0\";N;s:32:\"a06242fe4504c4a7a4fd36203e527bdd\";a:0:{}s:32:\"d9bd9ead19a12f2e3ec3365308607213\";a:0:{}s:32:\"6e64b59c86e6bdb6a0fa26120404c90c\";a:0:{}s:32:\"6e77b4db4307aad474f13d897e830499\";a:0:{}s:32:\"7432dac0107bc408a25a91a6df0f3c5b\";a:0:{}s:32:\"c5fa56bb8c1f412a6d2777c8142e3608\";a:0:{}s:32:\"53c929f181b7a1d2faf6383d9a0dedff\";a:0:{}s:32:\"f07c38e47e6b5a2b610722cf3b67fa00\";N;s:32:\"fcdff9d5d007e29eb75de8194b8d4c3a\";a:0:{}s:32:\"0661c4e579e19ec28921d53393b1f8b0\";a:0:{}s:32:\"54f4a0e29a03d6f70bc219dfdd234caf\";a:0:{}s:32:\"ee8a557109f3a482c273f055421564bd\";a:0:{}s:32:\"e0f76ebc08d523d559bcb8accd814f4c\";a:0:{}s:32:\"22c7a6d509efa958de8a11fed94cb376\";a:0:{}s:32:\"a9dd24eee477a00788d86dab2cc8eaf2\";a:0:{}s:32:\"7593be111a2edb249128e96262e71de5\";N;s:32:\"94b0be5716c8405bb4811350b67b969d\";a:0:{}s:32:\"89cf514acf1edcf12e1e07bf77e9d255\";a:0:{}s:32:\"b253ec6316581a6d23eedbe5d700d19a\";a:0:{}s:32:\"d7f0e03feef7ab486d07b7b5185345e9\";a:0:{}s:32:\"c889675d0463f48572a3a43770a0dc13\";a:0:{}s:32:\"1069a5aa1a9c16b67babec281af049a8\";a:0:{}s:32:\"9c380d67352aeabf857b496b6af2fb02\";a:0:{}s:32:\"353859f25f7dd524e716e687b41b0889\";N;s:32:\"70699a4434a5c07fca4647fc4da17155\";a:0:{}s:32:\"794c2590541119d9848c928779427cdb\";a:0:{}s:32:\"2e9ec318db459927717c36e3eab47120\";a:0:{}s:32:\"24c96b3dd0c49527e10634dd06c87d42\";a:0:{}s:32:\"dd511940ccd306ac21071c9904e668f2\";a:0:{}s:32:\"4b6702cbaf893cab8dbeeec34c15c6a9\";a:0:{}s:32:\"32e40f841ad6790bc624ff5c81abaf80\";a:0:{}s:32:\"91d92237f17cda9a4d1a5a17f9072746\";N;s:32:\"c26b5953ada755bbe106eedb89c1a557\";a:0:{}s:32:\"aa8d3e333ab56981d9c3837a34a0cb37\";a:0:{}s:32:\"fb4c9cb372df4aafca165e6936d6da96\";a:0:{}s:32:\"e3d1ab54087325dd2ff5449f5d7c861a\";a:0:{}s:32:\"b17c156b31832eeaf339923ce0dcb874\";a:0:{}s:32:\"70f305a44d5bc3aa5ba9eff188d5f43d\";a:0:{}s:32:\"86f4d8fddf25087de44fd59260d9b926\";a:0:{}s:32:\"5a5b9aa14f6486e263cf83f45484a2b1\";N;s:32:\"c60c677176909d0596da83adca30cc41\";a:0:{}s:32:\"cdeaea04a6d93837f3de51bf686dbb64\";a:0:{}s:32:\"ec3434f86080c5c546efd58d2a1d27ce\";a:0:{}s:32:\"6a034c845121bbc587c43d03a1a1b31c\";a:0:{}s:32:\"68c6ee503c9a207e2c3b97fd1d2146a2\";a:0:{}s:32:\"2c8570f392bde587daca92bb2b943091\";a:0:{}s:32:\"f866e321764976d81f4d7e7edd98f04c\";a:0:{}s:32:\"b108d0ecca69457537bef68feb74628b\";N;s:32:\"a383902f8d2eb1d5704da56eb63f0c36\";a:0:{}s:32:\"275676348c55a78636231fce13b7a0af\";a:0:{}s:32:\"3da9c749795b714eaddf9902762ceb30\";a:0:{}s:32:\"676661e7903d595cddaa8b230a5c8e9e\";a:0:{}s:32:\"9cedb17c72cbb71b89243d9ec86fde3c\";a:0:{}s:32:\"55e4b5c41611ac30855dcf549a466781\";a:0:{}s:32:\"d384fe671df78338b3f241890504e85a\";a:0:{}s:32:\"f9a9bc95bffa7654de40f67732eaac3d\";N;s:32:\"9d012c233ec9b689b626dba49c507fef\";a:0:{}s:32:\"b5aefa5306eb4d1bfb49b7c18a15d1d8\";a:0:{}s:32:\"94a832300447a8c4faefc7a16f5db222\";a:0:{}s:32:\"b2259ed9bfc158f0c7a4ea8acb6577f5\";a:0:{}s:32:\"42e110c1e178a04aa30021848258dbcc\";a:0:{}s:32:\"2f2a033a3a838b90bfc4b5cb7fd5fb89\";a:0:{}s:32:\"1ca4151b4a9fbb562f4df5cbc75c8281\";a:0:{}s:32:\"d051b1c968badd44cde2db300d93680c\";N;s:32:\"1613802bba9e5502e7f585fa2e7751a7\";a:0:{}s:32:\"fd278ed1e57eab55cdc941c931610c99\";a:0:{}s:32:\"e249e1ee8bd7b8b59bc42cf5d8151961\";a:0:{}s:32:\"2e7120a8fc7dbcb639f9680f01566e00\";a:0:{}s:32:\"b36d4724f396fe5a27686a853455c8a4\";a:0:{}s:32:\"edd0fb100bb3ebb584634a9bc8c48438\";a:0:{}s:32:\"ff60468e5ea52f24207df5f558301e04\";a:0:{}s:32:\"6d3d7c251055c69269f47ec417e4b745\";N;s:32:\"991a2a9e44810f80c9fe280f6f8c2ca8\";a:0:{}s:32:\"81c4df976ee74cb57aea09c4ece4a033\";a:0:{}s:32:\"97f72b1f15a2be2b502ceae04e8c5034\";a:0:{}s:32:\"250594754ac9288899642caf9479c7b3\";a:0:{}}', 'no'),
(266, '_transient_timeout_wc_admin_report', '1525213037', 'no'),
(267, '_transient_wc_admin_report', 'a:2:{s:32:\"7d9b303d9101c4bcbca662128e1e2732\";a:0:{}s:32:\"5bc52e707d5aeb84c63fd6bfe8da10bb\";a:0:{}}', 'no'),
(272, '_transient_timeout_external_ip_address_::1', '1525687423', 'no'),
(273, '_transient_external_ip_address_::1', '503 Over Quota Error &nbsp; Over Quota This application is temporarily over its serving quota. Please try again later.', 'no'),
(276, '_transient_timeout__woocommerce_helper_updates', '1525126013', 'no'),
(277, '_transient__woocommerce_helper_updates', 'a:4:{s:4:\"hash\";s:32:\"d751713988987e9331980363e24189ce\";s:7:\"updated\";i:1525082813;s:8:\"products\";a:0:{}s:6:\"errors\";a:1:{i:0;s:10:\"http-error\";}}', 'no'),
(280, 'acf_version', '4.4.12', 'yes'),
(285, 'current_theme', 'Twenty Sixteen/twentysixteen-child', 'yes'),
(286, 'theme_mods_twentysixteen-child', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1525123341;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(287, 'theme_switched', '', 'yes'),
(288, 'woocommerce_maybe_regenerate_images_hash', '991b1ca641921cf0f5baf7a2fe85861b', 'yes'),
(291, '_site_transient_timeout_browser_cffcb8140921e0d14b31935178051df3', '1525688788', 'no'),
(292, '_site_transient_browser_cffcb8140921e0d14b31935178051df3', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"65.0.3325.181\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(294, 'theme_mods_twentysixteen', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1525084083;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(316, '_transient_timeout_external_ip_address_127.0.0.1', '1525698942', 'no'),
(317, '_transient_external_ip_address_127.0.0.1', '31.11.212.237', 'no'),
(346, '_transient_shipping-transient-version', '1525123329', 'yes'),
(349, '_transient_product_query-transient-version', '1525127421', 'yes'),
(350, '_transient_product-transient-version', '1525127421', 'yes'),
(371, 'woocommerce_product_type', 'both', 'yes'),
(372, 'woocommerce_allow_tracking', 'yes', 'yes');
INSERT INTO `rs_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(373, 'woocommerce_tracker_last_send', '1525123308', 'yes'),
(375, 'woocommerce_stripe_settings', 'a:3:{s:7:\"enabled\";s:3:\"yes\";s:14:\"create_account\";b:0;s:5:\"email\";s:21:\"pstruminski@gmail.com\";}', 'yes'),
(377, 'woocommerce_ppec_paypal_settings', 'a:2:{s:16:\"reroute_requests\";b:0;s:5:\"email\";s:21:\"pstruminski@gmail.com\";}', 'yes'),
(378, 'woocommerce_cheque_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(379, 'woocommerce_bacs_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(380, 'woocommerce_cod_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(381, 'wc_ppec_version', '1.5.3', 'yes'),
(384, '_site_transient_timeout_theme_roots', '1525125138', 'no'),
(385, '_site_transient_theme_roots', 'a:6:{s:5:\"risen\";s:7:\"/themes\";s:10:\"storefront\";s:7:\"/themes\";s:13:\"twentyfifteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:19:\"twentysixteen-child\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no'),
(387, 'wc_stripe_version', '4.1.2', 'yes'),
(388, 'woocommerce_flat_rate_1_settings', 'a:3:{s:5:\"title\";s:9:\"Flat rate\";s:10:\"tax_status\";s:7:\"taxable\";s:4:\"cost\";s:1:\"1\";}', 'yes'),
(389, 'woocommerce_flat_rate_2_settings', 'a:3:{s:5:\"title\";s:9:\"Flat rate\";s:10:\"tax_status\";s:7:\"taxable\";s:4:\"cost\";s:1:\"1\";}', 'yes'),
(392, 'theme_mods_storefront', 'a:7:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:17:\"storefront_styles\";s:5060:\"\n			.main-navigation ul li a,\n			.site-title a,\n			ul.menu li a,\n			.site-branding h1 a,\n			.site-footer .storefront-handheld-footer-bar a:not(.button),\n			button.menu-toggle,\n			button.menu-toggle:hover {\n				color: #333333;\n			}\n\n			button.menu-toggle,\n			button.menu-toggle:hover {\n				border-color: #333333;\n			}\n\n			.main-navigation ul li a:hover,\n			.main-navigation ul li:hover > a,\n			.site-title a:hover,\n			a.cart-contents:hover,\n			.site-header-cart .widget_shopping_cart a:hover,\n			.site-header-cart:hover > li > a,\n			.site-header ul.menu li.current-menu-item > a {\n				color: #747474;\n			}\n\n			table th {\n				background-color: #f8f8f8;\n			}\n\n			table tbody td {\n				background-color: #fdfdfd;\n			}\n\n			table tbody tr:nth-child(2n) td,\n			fieldset,\n			fieldset legend {\n				background-color: #fbfbfb;\n			}\n\n			.site-header,\n			.secondary-navigation ul ul,\n			.main-navigation ul.menu > li.menu-item-has-children:after,\n			.secondary-navigation ul.menu ul,\n			.storefront-handheld-footer-bar,\n			.storefront-handheld-footer-bar ul li > a,\n			.storefront-handheld-footer-bar ul li.search .site-search,\n			button.menu-toggle,\n			button.menu-toggle:hover {\n				background-color: #ffffff;\n			}\n\n			p.site-description,\n			.site-header,\n			.storefront-handheld-footer-bar {\n				color: #404040;\n			}\n\n			.storefront-handheld-footer-bar ul li.cart .count,\n			button.menu-toggle:after,\n			button.menu-toggle:before,\n			button.menu-toggle span:before {\n				background-color: #333333;\n			}\n\n			.storefront-handheld-footer-bar ul li.cart .count {\n				color: #ffffff;\n			}\n\n			.storefront-handheld-footer-bar ul li.cart .count {\n				border-color: #ffffff;\n			}\n\n			h1, h2, h3, h4, h5, h6 {\n				color: #333333;\n			}\n\n			.widget h1 {\n				border-bottom-color: #333333;\n			}\n\n			body,\n			.secondary-navigation a,\n			.onsale,\n			.pagination .page-numbers li .page-numbers:not(.current), .woocommerce-pagination .page-numbers li .page-numbers:not(.current) {\n				color: #6d6d6d;\n			}\n\n			.widget-area .widget a,\n			.hentry .entry-header .posted-on a,\n			.hentry .entry-header .byline a {\n				color: #727272;\n			}\n\n			a  {\n				color: #96588a;\n			}\n\n			a:focus,\n			.button:focus,\n			.button.alt:focus,\n			.button.added_to_cart:focus,\n			.button.wc-forward:focus,\n			button:focus,\n			input[type=\"button\"]:focus,\n			input[type=\"reset\"]:focus,\n			input[type=\"submit\"]:focus {\n				outline-color: #96588a;\n			}\n\n			button, input[type=\"button\"], input[type=\"reset\"], input[type=\"submit\"], .button, .added_to_cart, .widget a.button, .site-header-cart .widget_shopping_cart a.button {\n				background-color: #eeeeee;\n				border-color: #eeeeee;\n				color: #333333;\n			}\n\n			button:hover, input[type=\"button\"]:hover, input[type=\"reset\"]:hover, input[type=\"submit\"]:hover, .button:hover, .added_to_cart:hover, .widget a.button:hover, .site-header-cart .widget_shopping_cart a.button:hover {\n				background-color: #d5d5d5;\n				border-color: #d5d5d5;\n				color: #333333;\n			}\n\n			button.alt, input[type=\"button\"].alt, input[type=\"reset\"].alt, input[type=\"submit\"].alt, .button.alt, .added_to_cart.alt, .widget-area .widget a.button.alt, .added_to_cart, .widget a.button.checkout {\n				background-color: #333333;\n				border-color: #333333;\n				color: #ffffff;\n			}\n\n			button.alt:hover, input[type=\"button\"].alt:hover, input[type=\"reset\"].alt:hover, input[type=\"submit\"].alt:hover, .button.alt:hover, .added_to_cart.alt:hover, .widget-area .widget a.button.alt:hover, .added_to_cart:hover, .widget a.button.checkout:hover {\n				background-color: #1a1a1a;\n				border-color: #1a1a1a;\n				color: #ffffff;\n			}\n\n			.pagination .page-numbers li .page-numbers.current, .woocommerce-pagination .page-numbers li .page-numbers.current {\n				background-color: #e6e6e6;\n				color: #636363;\n			}\n\n			#comments .comment-list .comment-content .comment-text {\n				background-color: #f8f8f8;\n			}\n\n			.site-footer {\n				background-color: #f0f0f0;\n				color: #6d6d6d;\n			}\n\n			.site-footer a:not(.button) {\n				color: #333333;\n			}\n\n			.site-footer h1, .site-footer h2, .site-footer h3, .site-footer h4, .site-footer h5, .site-footer h6 {\n				color: #333333;\n			}\n\n			#order_review {\n				background-color: #ffffff;\n			}\n\n			#payment .payment_methods > li .payment_box,\n			#payment .place-order {\n				background-color: #fafafa;\n			}\n\n			#payment .payment_methods > li:not(.woocommerce-notice) {\n				background-color: #f5f5f5;\n			}\n\n			#payment .payment_methods > li:not(.woocommerce-notice):hover {\n				background-color: #f0f0f0;\n			}\n\n			@media screen and ( min-width: 768px ) {\n				.secondary-navigation ul.menu a:hover {\n					color: #595959;\n				}\n\n				.secondary-navigation ul.menu a {\n					color: #404040;\n				}\n\n				.site-header-cart .widget_shopping_cart,\n				.main-navigation ul.menu ul.sub-menu,\n				.main-navigation ul.nav-menu ul.children {\n					background-color: #f0f0f0;\n				}\n\n				.site-header-cart .widget_shopping_cart .buttons,\n				.site-header-cart .widget_shopping_cart .total {\n					background-color: #f5f5f5;\n				}\n\n				.site-header {\n					border-bottom-color: #f0f0f0;\n				}\n			}\";s:29:\"storefront_woocommerce_styles\";s:2283:\"\n			a.cart-contents,\n			.site-header-cart .widget_shopping_cart a {\n				color: #333333;\n			}\n\n			table.cart td.product-remove,\n			table.cart td.actions {\n				border-top-color: #ffffff;\n			}\n\n			.woocommerce-tabs ul.tabs li.active a,\n			ul.products li.product .price,\n			.onsale,\n			.widget_search form:before,\n			.widget_product_search form:before {\n				color: #6d6d6d;\n			}\n\n			.woocommerce-breadcrumb a,\n			a.woocommerce-review-link,\n			.product_meta a {\n				color: #727272;\n			}\n\n			.onsale {\n				border-color: #6d6d6d;\n			}\n\n			.star-rating span:before,\n			.quantity .plus, .quantity .minus,\n			p.stars a:hover:after,\n			p.stars a:after,\n			.star-rating span:before,\n			#payment .payment_methods li input[type=radio]:first-child:checked+label:before {\n				color: #96588a;\n			}\n\n			.widget_price_filter .ui-slider .ui-slider-range,\n			.widget_price_filter .ui-slider .ui-slider-handle {\n				background-color: #96588a;\n			}\n\n			.order_details {\n				background-color: #f8f8f8;\n			}\n\n			.order_details > li {\n				border-bottom: 1px dotted #e3e3e3;\n			}\n\n			.order_details:before,\n			.order_details:after {\n				background: -webkit-linear-gradient(transparent 0,transparent 0),-webkit-linear-gradient(135deg,#f8f8f8 33.33%,transparent 33.33%),-webkit-linear-gradient(45deg,#f8f8f8 33.33%,transparent 33.33%)\n			}\n\n			p.stars a:before,\n			p.stars a:hover~a:before,\n			p.stars.selected a.active~a:before {\n				color: #6d6d6d;\n			}\n\n			p.stars.selected a.active:before,\n			p.stars:hover a:before,\n			p.stars.selected a:not(.active):before,\n			p.stars.selected a.active:before {\n				color: #96588a;\n			}\n\n			.single-product div.product .woocommerce-product-gallery .woocommerce-product-gallery__trigger {\n				background-color: #eeeeee;\n				color: #333333;\n			}\n\n			.single-product div.product .woocommerce-product-gallery .woocommerce-product-gallery__trigger:hover {\n				background-color: #d5d5d5;\n				border-color: #d5d5d5;\n				color: #333333;\n			}\n\n			.button.loading {\n				color: #eeeeee;\n			}\n\n			.button.loading:hover {\n				background-color: #eeeeee;\n			}\n\n			.button.loading:after {\n				color: #333333;\n			}\n\n			@media screen and ( min-width: 768px ) {\n				.site-header-cart .widget_shopping_cart,\n				.site-header .product_list_widget li .quantity {\n					color: #404040;\n				}\n			}\";s:39:\"storefront_woocommerce_extension_styles\";s:0:\"\";s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1525124728;s:4:\"data\";a:7:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:8:\"header-1\";a:0:{}s:8:\"footer-1\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}s:8:\"footer-4\";a:0:{}}}}', 'yes'),
(393, 'storefront_nux_fresh_site', '0', 'yes'),
(394, 'woocommerce_catalog_rows', '4', 'yes'),
(395, 'woocommerce_catalog_columns', '3', 'yes'),
(405, '_transient_timeout_wc_shipping_method_count_1_1525123329', '1527715354', 'no'),
(406, '_transient_wc_shipping_method_count_1_1525123329', '2', 'no'),
(407, 'storefront_nux_dismissed', '1', 'yes'),
(411, '_transient_timeout_wc_addons_sections', '1525728316', 'no'),
(412, '_transient_wc_addons_sections', 'a:9:{i:0;O:8:\"stdClass\":2:{s:4:\"slug\";s:9:\"_featured\";s:5:\"label\";s:8:\"Featured\";}i:1;O:8:\"stdClass\":2:{s:4:\"slug\";s:4:\"_all\";s:5:\"label\";s:3:\"All\";}i:2;O:8:\"stdClass\":2:{s:4:\"slug\";s:7:\"bundles\";s:5:\"label\";s:7:\"Bundles\";}i:3;O:8:\"stdClass\":2:{s:4:\"slug\";s:18:\"product-extensions\";s:5:\"label\";s:12:\"Enhancements\";}i:4;O:8:\"stdClass\":2:{s:4:\"slug\";s:20:\"marketing-extensions\";s:5:\"label\";s:9:\"Marketing\";}i:5;O:8:\"stdClass\":2:{s:4:\"slug\";s:16:\"payment-gateways\";s:5:\"label\";s:8:\"Payments\";}i:6;O:8:\"stdClass\":2:{s:4:\"slug\";s:12:\"product-type\";s:5:\"label\";s:12:\"Product Type\";}i:7;O:8:\"stdClass\":2:{s:4:\"slug\";s:16:\"shipping-methods\";s:5:\"label\";s:8:\"Shipping\";}i:8;O:8:\"stdClass\":2:{s:4:\"slug\";s:10:\"operations\";s:5:\"label\";s:16:\"Store Management\";}}', 'no'),
(413, '_transient_timeout_wc_addons_featured', '1525728317', 'no'),
(414, '_transient_wc_addons_featured', 'O:8:\"stdClass\":1:{s:8:\"sections\";a:11:{i:0;O:8:\"stdClass\":4:{s:6:\"module\";s:12:\"banner_block\";s:5:\"title\";s:50:\"Take your store beyond the typical - sell anything\";s:11:\"description\";s:81:\"From services to content, there\'s no limit to what you can sell with WooCommerce.\";s:5:\"items\";a:9:{i:0;O:8:\"stdClass\":6:{s:4:\"href\";s:139:\"https://woocommerce.com/products/woocommerce-subscriptions/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:13:\"Subscriptions\";s:5:\"image\";s:71:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/subscriptions-icon@2x.png\";s:11:\"description\";s:98:\"Let customers subscribe to your products or services and pay on a weekly, monthly or annual basis.\";s:6:\"button\";s:10:\"From: $199\";s:6:\"plugin\";s:55:\"woocommerce-subscriptions/woocommerce-subscriptions.php\";}i:1;O:8:\"stdClass\":6:{s:4:\"href\";s:134:\"https://woocommerce.com/products/woocommerce-bookings/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:8:\"Bookings\";s:5:\"image\";s:66:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/bookings-icon@2x.png\";s:11:\"description\";s:76:\"Allow customers to book appointments for services without leaving your site.\";s:6:\"button\";s:10:\"From: $249\";s:6:\"plugin\";s:45:\"woocommerce-bookings/woocommerce-bookings.php\";}i:2;O:8:\"stdClass\":6:{s:4:\"href\";s:137:\"https://woocommerce.com/products/woocommerce-memberships/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:11:\"Memberships\";s:5:\"image\";s:69:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/memberships-icon@2x.png\";s:11:\"description\";s:76:\"Give members access to restricted content or products, for a fee or for free\";s:6:\"button\";s:10:\"From: $149\";s:6:\"plugin\";s:51:\"woocommerce-memberships/woocommerce-memberships.php\";}i:3;O:8:\"stdClass\":6:{s:4:\"href\";s:129:\"https://woocommerce.com/products/product-bundles/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:15:\"Product Bundles\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:50:\"Offer customizable bundles and assembled products.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:59:\"woocommerce-product-bundles/woocommerce-product-bundles.php\";}i:4;O:8:\"stdClass\":6:{s:4:\"href\";s:132:\"https://woocommerce.com/products/composite-products/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:18:\"Composite Products\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:59:\"Create and offer product kits with configurable components.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:65:\"woocommerce-composite-products/woocommerce-composite-products.php\";}i:5;O:8:\"stdClass\":6:{s:4:\"href\";s:129:\"https://woocommerce.com/products/product-vendors/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:15:\"Product Vendors\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:47:\"Turn your store into a multi-vendor marketplace\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:59:\"woocommerce-product-vendors/woocommerce-product-vendors.php\";}i:6;O:8:\"stdClass\":6:{s:4:\"href\";s:132:\"https://woocommerce.com/products/groups-woocommerce/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:22:\"Groups for WooCommerce\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:94:\"Sell memberships using the free &#039;Groups&#039; plugin, Groups integration and WooCommerce.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:41:\"groups-woocommerce/groups-woocommerce.php\";}i:7;O:8:\"stdClass\":6:{s:4:\"href\";s:136:\"https://woocommerce.com/products/woocommerce-pre-orders/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:22:\"WooCommerce Pre-Orders\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:60:\"Allow customers to order products before they are available.\";s:6:\"button\";s:10:\"From: $129\";s:6:\"plugin\";s:49:\"woocommerce-pre-orders/woocommerce-pre-orders.php\";}i:8;O:8:\"stdClass\":6:{s:4:\"href\";s:130:\"https://woocommerce.com/products/chained-products/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:16:\"Chained Products\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:69:\"Create and sell pre-configured product bundles and discounted combos.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:61:\"woocommerce-chained-products/woocommerce-chained-products.php\";}}}i:1;O:8:\"stdClass\":1:{s:6:\"module\";s:16:\"wcs_banner_block\";}i:2;O:8:\"stdClass\":2:{s:6:\"module\";s:12:\"column_start\";s:9:\"container\";s:22:\"column_container_start\";}i:3;O:8:\"stdClass\":4:{s:6:\"module\";s:12:\"column_block\";s:5:\"title\";s:46:\"Improve the main features of your online store\";s:11:\"description\";s:71:\"Sell more by helping customers find the products and options they want.\";s:5:\"items\";a:9:{i:0;O:8:\"stdClass\":6:{s:4:\"href\";s:129:\"https://woocommerce.com/products/product-add-ons/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:15:\"Product Add-ons\";s:5:\"image\";s:73:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/product-add-ons-icon@2x.png\";s:11:\"description\";s:82:\"Give your customers the option to customize their purchase or add personalization.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:57:\"woocommerce-product-addons/woocommerce-product-addons.php\";}i:1;O:8:\"stdClass\":6:{s:4:\"href\";s:140:\"https://woocommerce.com/products/woocommerce-product-search/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:14:\"Product Search\";s:5:\"image\";s:72:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/product-search-icon@2x.png\";s:11:\"description\";s:67:\"Make sure customers find what they want when they search your site.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:57:\"woocommerce-product-search/woocommerce-product-search.php\";}i:2;O:8:\"stdClass\":6:{s:4:\"href\";s:142:\"https://woocommerce.com/products/woocommerce-checkout-add-ons/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:16:\"Checkout Add-ons\";s:5:\"image\";s:74:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/checkout-add-ons-icon@2x.png\";s:11:\"description\";s:89:\"Highlight relevant products, offers like free shipping and other upsells during checkout.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:61:\"woocommerce-checkout-add-ons/woocommerce-checkout-add-ons.php\";}i:3;O:8:\"stdClass\":6:{s:4:\"href\";s:147:\"https://woocommerce.com/products/woocommerce-checkout-field-editor/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:21:\"Checkout Field Editor\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:128:\"The checkout field editor provides you with an interface to add, edit and remove fields shown on your WooCommerce checkout page.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:71:\"woocommerce-checkout-field-editor/woocommerce-checkout-field-editor.php\";}i:4;O:8:\"stdClass\":6:{s:4:\"href\";s:138:\"https://woocommerce.com/products/woocommerce-social-login/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:24:\"WooCommerce Social Login\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:62:\"Enable Social Login for Seamless Checkout and Account Creation\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:53:\"woocommerce-social-login/woocommerce-social-login.php\";}i:5;O:8:\"stdClass\":6:{s:4:\"href\";s:135:\"https://woocommerce.com/products/woocommerce-wishlists/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:21:\"WooCommerce Wishlists\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:113:\"WooCommerce Wishlists allows guests and customers to create and add products to an unlimited number of Wishlists.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:47:\"woocommerce-wishlists/woocommerce-wishlists.php\";}i:6;O:8:\"stdClass\":6:{s:4:\"href\";s:126:\"https://woocommerce.com/products/cart-notices/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:12:\"Cart Notices\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:73:\"Display dynamic, actionable messages to your customers as they check out.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:53:\"woocommerce-cart-notices/woocommerce-cart-notices.php\";}i:7;O:8:\"stdClass\":6:{s:4:\"href\";s:126:\"https://woocommerce.com/products/cart-add-ons/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:12:\"Cart Add-ons\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:109:\"A powerful tool for driving incremental and impulse purchases by customers once they are in the shopping cart\";s:6:\"button\";s:9:\"From: $29\";s:6:\"plugin\";s:53:\"woocommerce-cart-add-ons/woocommerce-cart-add-ons.php\";}i:8;O:8:\"stdClass\":6:{s:4:\"href\";s:134:\"https://woocommerce.com/products/woocommerce-waitlist/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:20:\"WooCommerce Waitlist\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:117:\"With WooCommerce Waitlist customers can register for email notifications when out-of-stock products become available.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:45:\"woocommerce-waitlist/woocommerce-waitlist.php\";}}}i:4;O:8:\"stdClass\":5:{s:6:\"module\";s:17:\"small_light_block\";s:5:\"title\";s:34:\"Get the official WooCommerce theme\";s:11:\"description\";s:128:\"Storefront is the lean, flexible, and free theme, built by the people who make WooCommerce - everything you need to get started.\";s:5:\"image\";s:70:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/storefront-screen@2x.png\";s:7:\"buttons\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"href\";s:44:\"/wp-admin/theme-install.php?theme=storefront\";s:4:\"text\";s:7:\"Install\";}i:1;O:8:\"stdClass\":2:{s:4:\"href\";s:115:\"https://woocommerce.com/storefront/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:4:\"text\";s:9:\"Read More\";}}}i:5;O:8:\"stdClass\":1:{s:6:\"module\";s:10:\"column_end\";}i:6;O:8:\"stdClass\":1:{s:6:\"module\";s:12:\"column_start\";}i:7;O:8:\"stdClass\":4:{s:6:\"module\";s:16:\"small_dark_block\";s:5:\"title\";s:20:\"Square + WooCommerce\";s:11:\"description\";s:176:\"Keep your WooCommerce and brick-and-mortar stores in sync. Use Square to take payments both online and offline, keep inventory updated between the two and sync product changes.\";s:5:\"items\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"href\";s:120:\"https://woocommerce.com/products/square/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:6:\"button\";s:5:\"Free!\";}}}i:8;O:8:\"stdClass\":4:{s:6:\"module\";s:12:\"column_block\";s:5:\"title\";s:19:\"Get deeper insights\";s:11:\"description\";s:58:\"Learn how your store is performing with enhanced reporting\";s:5:\"items\";a:8:{i:0;O:8:\"stdClass\":6:{s:4:\"href\";s:142:\"https://woocommerce.com/products/woocommerce-google-analytics/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:16:\"Google Analytics\";s:5:\"image\";s:60:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/ga-icon@2x.png\";s:11:\"description\";s:93:\"Understand your customers and increase revenue with the world’s leading analytics platform.\";s:6:\"button\";s:4:\"Free\";s:6:\"plugin\";s:85:\"woocommerce-google-analytics-integration/woocommerce-google-analytics-integration.php\";}i:1;O:8:\"stdClass\":6:{s:4:\"href\";s:138:\"https://woocommerce.com/products/woocommerce-cart-reports/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:12:\"Cart reports\";s:5:\"image\";s:70:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/cart-reports-icon@2x.png\";s:11:\"description\";s:66:\"Get real-time reports on what customers are leaving in their cart.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:53:\"woocommerce-cart-reports/woocommerce-cart-reports.php\";}i:2;O:8:\"stdClass\":6:{s:4:\"href\";s:139:\"https://woocommerce.com/products/woocommerce-cost-of-goods/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:13:\"Cost of Goods\";s:5:\"image\";s:71:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/cost-of-goods-icon@2x.png\";s:11:\"description\";s:64:\"Easily track profit by including  cost of goods in your reports.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:55:\"woocommerce-cost-of-goods/woocommerce-cost-of-goods.php\";}i:3;O:8:\"stdClass\":6:{s:4:\"href\";s:146:\"https://woocommerce.com/products/woocommerce-google-analytics-pro/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:32:\"WooCommerce Google Analytics Pro\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:85:\"Add advanced event tracking and enhanced eCommerce tracking to your WooCommerce site.\";s:6:\"button\";s:9:\"From: $29\";s:6:\"plugin\";s:69:\"woocommerce-google-analytics-pro/woocommerce-google-analytics-pro.php\";}i:4;O:8:\"stdClass\":6:{s:4:\"href\";s:142:\"https://woocommerce.com/products/woocommerce-customer-history/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:28:\"WooCommerce Customer History\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:125:\"Observe how your customers use your store, keep a full purchase history log, and calculate the total customer lifetime value.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:61:\"woocommerce-customer-history/woocommerce-customer-history.php\";}i:5;O:8:\"stdClass\":6:{s:4:\"href\";s:126:\"https://woocommerce.com/products/kiss-metrics/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:11:\"Kissmetrics\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:79:\"Easily add Kissmetrics event tracking to your WooCommerce store with one click.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:52:\"woocommerce-kiss-metrics/woocommerce-kissmetrics.php\";}i:6;O:8:\"stdClass\":6:{s:4:\"href\";s:122:\"https://woocommerce.com/products/mixpanel/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:8:\"Mixpanel\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:65:\"Add event tracking powered by Mixpanel to your WooCommerce store.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:45:\"woocommerce-mixpanel/woocommerce-mixpanel.php\";}i:7;O:8:\"stdClass\":6:{s:4:\"href\";s:144:\"https://woocommerce.com/products/woocommerce-sales-report-email/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:30:\"WooCommerce Sales Report Email\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:107:\"Receive emails daily, weekly or monthly with meaningful information about how your products are performing.\";s:6:\"button\";s:9:\"From: $29\";s:6:\"plugin\";s:65:\"woocommerce-sales-report-email/woocommerce-sales-report-email.php\";}}}i:9;O:8:\"stdClass\":2:{s:6:\"module\";s:10:\"column_end\";s:9:\"container\";s:20:\"column_container_end\";}i:10;O:8:\"stdClass\":4:{s:6:\"module\";s:12:\"banner_block\";s:5:\"title\";s:40:\"Promote your products and increase sales\";s:11:\"description\";s:77:\"From coupons to emails, these extensions can power up your marketing efforts.\";s:5:\"items\";a:9:{i:0;O:8:\"stdClass\":6:{s:4:\"href\";s:127:\"https://woocommerce.com/products/smart-coupons/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:13:\"Smart Coupons\";s:5:\"image\";s:71:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/smart-coupons-icon@2x.png\";s:11:\"description\";s:106:\"Enhance your coupon options - create gift certificates, store credit, coupons based on purchases and more.\";s:6:\"button\";s:9:\"From: $99\";s:6:\"plugin\";s:55:\"woocommerce-smart-coupons/woocommerce-smart-coupons.php\";}i:1;O:8:\"stdClass\":6:{s:4:\"href\";s:130:\"https://woocommerce.com/products/follow-up-emails/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:16:\"Follow Up Emails\";s:5:\"image\";s:74:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/follow-up-emails-icon@2x.png\";s:11:\"description\";s:140:\"Automatically contact customers after purchase - be it everyone, your most loyal or your biggest spenders - and keep your store top-of-mind.\";s:6:\"button\";s:9:\"From: $99\";s:6:\"plugin\";s:61:\"woocommerce-follow-up-emails/woocommerce-follow-up-emails.php\";}i:2;O:8:\"stdClass\":6:{s:4:\"href\";s:133:\"https://woocommerce.com/products/google-product-feed/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:19:\"Google Product Feed\";s:5:\"image\";s:77:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/google-product-feed-icon@2x.png\";s:11:\"description\";s:61:\"Let customers find you when shopping for products via Google.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:45:\"woocommerce-product-feeds/woocommerce-gpf.php\";}i:3;O:8:\"stdClass\":6:{s:4:\"href\";s:129:\"https://woocommerce.com/products/dynamic-pricing/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:15:\"Dynamic Pricing\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:48:\"Bulk discounts, role-based pricing and much more\";s:6:\"button\";s:10:\"From: $129\";s:6:\"plugin\";s:59:\"woocommerce-dynamic-pricing/woocommerce-dynamic-pricing.php\";}i:4;O:8:\"stdClass\":6:{s:4:\"href\";s:144:\"https://woocommerce.com/products/woocommerce-points-and-rewards/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:30:\"WooCommerce Points and Rewards\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:102:\"Reward your customers for purchases and other actions with points which can be redeemed for discounts.\";s:6:\"button\";s:10:\"From: $129\";s:6:\"plugin\";s:65:\"woocommerce-points-and-rewards/woocommerce-points-and-rewards.php\";}i:5;O:8:\"stdClass\":6:{s:4:\"href\";s:126:\"https://woocommerce.com/products/store-credit/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:24:\"WooCommerce Store Credit\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:152:\"Generate store credit coupons that enable customers to make multiple purchases until the total value specified is exhausted or the coupons life expires.\";s:6:\"button\";s:9:\"From: $29\";s:6:\"plugin\";s:53:\"woocommerce-store-credit/woocommerce-store-credit.php\";}i:6;O:8:\"stdClass\":6:{s:4:\"href\";s:122:\"https://woocommerce.com/products/facebook/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:24:\"Facebook for WooCommerce\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:89:\"Get the Facebook for WooCommerce plugin for two powerful ways to help grow your business.\";s:6:\"button\";s:4:\"Free\";s:6:\"plugin\";s:53:\"facebook-for-woocommerce/facebook-for-woocommerce.php\";}i:7;O:8:\"stdClass\":6:{s:4:\"href\";s:137:\"https://woocommerce.com/products/newsletter-subscription/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:23:\"Newsletter Subscription\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:127:\"Allow customers to subscribe to your MailChimp or CampaignMonitor mailing list(s) via a widget or by opting in during checkout.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:63:\"woocommerce-subscribe-to-newsletter/subscribe-to-newsletter.php\";}i:8;O:8:\"stdClass\":6:{s:4:\"href\";s:142:\"https://woocommerce.com/products/woocommerce-email-customizer/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:28:\"WooCommerce Email Customizer\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:125:\"Connect with your customers with each email you send by visually modifying your email templates via the WordPress Customizer.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:61:\"woocommerce-email-customizer/woocommerce-email-customizer.php\";}}}}}', 'no'),
(415, 'storefront_nux_guided_tour', '1', 'yes'),
(418, '_transient_timeout_wc_shipping_method_count_0_1525123329', '1527715755', 'no'),
(419, '_transient_wc_shipping_method_count_0_1525123329', '2', 'no'),
(420, 'wc_gateway_ppce_bootstrap_warning_message', 'WooCommerce Gateway PayPal Express Checkout requires WooCommerce to be activated', 'yes'),
(421, 'wc_gateway_ppec_bootstrap_warning_message_dismissed', 'yes', 'yes'),
(423, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1525134946', 'no'),
(424, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'O:8:\"stdClass\":100:{s:6:\"widget\";a:3:{s:4:\"name\";s:6:\"widget\";s:4:\"slug\";s:6:\"widget\";s:5:\"count\";i:4459;}s:11:\"woocommerce\";a:3:{s:4:\"name\";s:11:\"woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:5:\"count\";i:2750;}s:4:\"post\";a:3:{s:4:\"name\";s:4:\"post\";s:4:\"slug\";s:4:\"post\";s:5:\"count\";i:2556;}s:5:\"admin\";a:3:{s:4:\"name\";s:5:\"admin\";s:4:\"slug\";s:5:\"admin\";s:5:\"count\";i:2422;}s:5:\"posts\";a:3:{s:4:\"name\";s:5:\"posts\";s:4:\"slug\";s:5:\"posts\";s:5:\"count\";i:1867;}s:9:\"shortcode\";a:3:{s:4:\"name\";s:9:\"shortcode\";s:4:\"slug\";s:9:\"shortcode\";s:5:\"count\";i:1648;}s:8:\"comments\";a:3:{s:4:\"name\";s:8:\"comments\";s:4:\"slug\";s:8:\"comments\";s:5:\"count\";i:1639;}s:7:\"twitter\";a:3:{s:4:\"name\";s:7:\"twitter\";s:4:\"slug\";s:7:\"twitter\";s:5:\"count\";i:1451;}s:6:\"images\";a:3:{s:4:\"name\";s:6:\"images\";s:4:\"slug\";s:6:\"images\";s:5:\"count\";i:1387;}s:6:\"google\";a:3:{s:4:\"name\";s:6:\"google\";s:4:\"slug\";s:6:\"google\";s:5:\"count\";i:1386;}s:8:\"facebook\";a:3:{s:4:\"name\";s:8:\"facebook\";s:4:\"slug\";s:8:\"facebook\";s:5:\"count\";i:1383;}s:5:\"image\";a:3:{s:4:\"name\";s:5:\"image\";s:4:\"slug\";s:5:\"image\";s:5:\"count\";i:1310;}s:7:\"sidebar\";a:3:{s:4:\"name\";s:7:\"sidebar\";s:4:\"slug\";s:7:\"sidebar\";s:5:\"count\";i:1283;}s:3:\"seo\";a:3:{s:4:\"name\";s:3:\"seo\";s:4:\"slug\";s:3:\"seo\";s:5:\"count\";i:1200;}s:7:\"gallery\";a:3:{s:4:\"name\";s:7:\"gallery\";s:4:\"slug\";s:7:\"gallery\";s:5:\"count\";i:1099;}s:4:\"page\";a:3:{s:4:\"name\";s:4:\"page\";s:4:\"slug\";s:4:\"page\";s:5:\"count\";i:1059;}s:6:\"social\";a:3:{s:4:\"name\";s:6:\"social\";s:4:\"slug\";s:6:\"social\";s:5:\"count\";i:1022;}s:5:\"email\";a:3:{s:4:\"name\";s:5:\"email\";s:4:\"slug\";s:5:\"email\";s:5:\"count\";i:1008;}s:9:\"ecommerce\";a:3:{s:4:\"name\";s:9:\"ecommerce\";s:4:\"slug\";s:9:\"ecommerce\";s:5:\"count\";i:884;}s:5:\"login\";a:3:{s:4:\"name\";s:5:\"login\";s:4:\"slug\";s:5:\"login\";s:5:\"count\";i:875;}s:5:\"links\";a:3:{s:4:\"name\";s:5:\"links\";s:4:\"slug\";s:5:\"links\";s:5:\"count\";i:826;}s:5:\"video\";a:3:{s:4:\"name\";s:5:\"video\";s:4:\"slug\";s:5:\"video\";s:5:\"count\";i:802;}s:7:\"widgets\";a:3:{s:4:\"name\";s:7:\"widgets\";s:4:\"slug\";s:7:\"widgets\";s:5:\"count\";i:801;}s:8:\"security\";a:3:{s:4:\"name\";s:8:\"security\";s:4:\"slug\";s:8:\"security\";s:5:\"count\";i:706;}s:7:\"content\";a:3:{s:4:\"name\";s:7:\"content\";s:4:\"slug\";s:7:\"content\";s:5:\"count\";i:691;}s:3:\"rss\";a:3:{s:4:\"name\";s:3:\"rss\";s:4:\"slug\";s:3:\"rss\";s:5:\"count\";i:684;}s:10:\"buddypress\";a:3:{s:4:\"name\";s:10:\"buddypress\";s:4:\"slug\";s:10:\"buddypress\";s:5:\"count\";i:682;}s:4:\"spam\";a:3:{s:4:\"name\";s:4:\"spam\";s:4:\"slug\";s:4:\"spam\";s:5:\"count\";i:672;}s:6:\"slider\";a:3:{s:4:\"name\";s:6:\"slider\";s:4:\"slug\";s:6:\"slider\";s:5:\"count\";i:655;}s:5:\"pages\";a:3:{s:4:\"name\";s:5:\"pages\";s:4:\"slug\";s:5:\"pages\";s:5:\"count\";i:655;}s:9:\"analytics\";a:3:{s:4:\"name\";s:9:\"analytics\";s:4:\"slug\";s:9:\"analytics\";s:5:\"count\";i:641;}s:6:\"jquery\";a:3:{s:4:\"name\";s:6:\"jquery\";s:4:\"slug\";s:6:\"jquery\";s:5:\"count\";i:639;}s:5:\"media\";a:3:{s:4:\"name\";s:5:\"media\";s:4:\"slug\";s:5:\"media\";s:5:\"count\";i:634;}s:10:\"e-commerce\";a:3:{s:4:\"name\";s:10:\"e-commerce\";s:4:\"slug\";s:10:\"e-commerce\";s:5:\"count\";i:625;}s:4:\"feed\";a:3:{s:4:\"name\";s:4:\"feed\";s:4:\"slug\";s:4:\"feed\";s:5:\"count\";i:610;}s:6:\"search\";a:3:{s:4:\"name\";s:6:\"search\";s:4:\"slug\";s:6:\"search\";s:5:\"count\";i:606;}s:4:\"ajax\";a:3:{s:4:\"name\";s:4:\"ajax\";s:4:\"slug\";s:4:\"ajax\";s:5:\"count\";i:603;}s:4:\"form\";a:3:{s:4:\"name\";s:4:\"form\";s:4:\"slug\";s:4:\"form\";s:5:\"count\";i:594;}s:8:\"category\";a:3:{s:4:\"name\";s:8:\"category\";s:4:\"slug\";s:8:\"category\";s:5:\"count\";i:588;}s:4:\"menu\";a:3:{s:4:\"name\";s:4:\"menu\";s:4:\"slug\";s:4:\"menu\";s:5:\"count\";i:585;}s:5:\"embed\";a:3:{s:4:\"name\";s:5:\"embed\";s:4:\"slug\";s:5:\"embed\";s:5:\"count\";i:563;}s:10:\"javascript\";a:3:{s:4:\"name\";s:10:\"javascript\";s:4:\"slug\";s:10:\"javascript\";s:5:\"count\";i:545;}s:4:\"link\";a:3:{s:4:\"name\";s:4:\"link\";s:4:\"slug\";s:4:\"link\";s:5:\"count\";i:536;}s:3:\"css\";a:3:{s:4:\"name\";s:3:\"css\";s:4:\"slug\";s:3:\"css\";s:5:\"count\";i:531;}s:7:\"youtube\";a:3:{s:4:\"name\";s:7:\"youtube\";s:4:\"slug\";s:7:\"youtube\";s:5:\"count\";i:521;}s:5:\"share\";a:3:{s:4:\"name\";s:5:\"share\";s:4:\"slug\";s:5:\"share\";s:5:\"count\";i:519;}s:7:\"comment\";a:3:{s:4:\"name\";s:7:\"comment\";s:4:\"slug\";s:7:\"comment\";s:5:\"count\";i:511;}s:5:\"theme\";a:3:{s:4:\"name\";s:5:\"theme\";s:4:\"slug\";s:5:\"theme\";s:5:\"count\";i:505;}s:9:\"dashboard\";a:3:{s:4:\"name\";s:9:\"dashboard\";s:4:\"slug\";s:9:\"dashboard\";s:5:\"count\";i:492;}s:6:\"editor\";a:3:{s:4:\"name\";s:6:\"editor\";s:4:\"slug\";s:6:\"editor\";s:5:\"count\";i:491;}s:10:\"responsive\";a:3:{s:4:\"name\";s:10:\"responsive\";s:4:\"slug\";s:10:\"responsive\";s:5:\"count\";i:489;}s:6:\"custom\";a:3:{s:4:\"name\";s:6:\"custom\";s:4:\"slug\";s:6:\"custom\";s:5:\"count\";i:482;}s:10:\"categories\";a:3:{s:4:\"name\";s:10:\"categories\";s:4:\"slug\";s:10:\"categories\";s:5:\"count\";i:479;}s:12:\"contact-form\";a:3:{s:4:\"name\";s:12:\"contact form\";s:4:\"slug\";s:12:\"contact-form\";s:5:\"count\";i:477;}s:3:\"ads\";a:3:{s:4:\"name\";s:3:\"ads\";s:4:\"slug\";s:3:\"ads\";s:5:\"count\";i:469;}s:9:\"affiliate\";a:3:{s:4:\"name\";s:9:\"affiliate\";s:4:\"slug\";s:9:\"affiliate\";s:5:\"count\";i:465;}s:6:\"button\";a:3:{s:4:\"name\";s:6:\"button\";s:4:\"slug\";s:6:\"button\";s:5:\"count\";i:456;}s:4:\"tags\";a:3:{s:4:\"name\";s:4:\"tags\";s:4:\"slug\";s:4:\"tags\";s:5:\"count\";i:454;}s:4:\"user\";a:3:{s:4:\"name\";s:4:\"user\";s:4:\"slug\";s:4:\"user\";s:5:\"count\";i:443;}s:7:\"contact\";a:3:{s:4:\"name\";s:7:\"contact\";s:4:\"slug\";s:7:\"contact\";s:5:\"count\";i:432;}s:6:\"mobile\";a:3:{s:4:\"name\";s:6:\"mobile\";s:4:\"slug\";s:6:\"mobile\";s:5:\"count\";i:425;}s:3:\"api\";a:3:{s:4:\"name\";s:3:\"api\";s:4:\"slug\";s:3:\"api\";s:5:\"count\";i:424;}s:5:\"photo\";a:3:{s:4:\"name\";s:5:\"photo\";s:4:\"slug\";s:5:\"photo\";s:5:\"count\";i:419;}s:5:\"users\";a:3:{s:4:\"name\";s:5:\"users\";s:4:\"slug\";s:5:\"users\";s:5:\"count\";i:415;}s:9:\"slideshow\";a:3:{s:4:\"name\";s:9:\"slideshow\";s:4:\"slug\";s:9:\"slideshow\";s:5:\"count\";i:413;}s:5:\"stats\";a:3:{s:4:\"name\";s:5:\"stats\";s:4:\"slug\";s:5:\"stats\";s:5:\"count\";i:412;}s:6:\"events\";a:3:{s:4:\"name\";s:6:\"events\";s:4:\"slug\";s:6:\"events\";s:5:\"count\";i:406;}s:6:\"photos\";a:3:{s:4:\"name\";s:6:\"photos\";s:4:\"slug\";s:6:\"photos\";s:5:\"count\";i:404;}s:10:\"statistics\";a:3:{s:4:\"name\";s:10:\"statistics\";s:4:\"slug\";s:10:\"statistics\";s:5:\"count\";i:390;}s:7:\"payment\";a:3:{s:4:\"name\";s:7:\"payment\";s:4:\"slug\";s:7:\"payment\";s:5:\"count\";i:388;}s:10:\"navigation\";a:3:{s:4:\"name\";s:10:\"navigation\";s:4:\"slug\";s:10:\"navigation\";s:5:\"count\";i:384;}s:4:\"news\";a:3:{s:4:\"name\";s:4:\"news\";s:4:\"slug\";s:4:\"news\";s:5:\"count\";i:365;}s:8:\"calendar\";a:3:{s:4:\"name\";s:8:\"calendar\";s:4:\"slug\";s:8:\"calendar\";s:5:\"count\";i:363;}s:10:\"shortcodes\";a:3:{s:4:\"name\";s:10:\"shortcodes\";s:4:\"slug\";s:10:\"shortcodes\";s:5:\"count\";i:357;}s:5:\"popup\";a:3:{s:4:\"name\";s:5:\"popup\";s:4:\"slug\";s:5:\"popup\";s:5:\"count\";i:354;}s:9:\"marketing\";a:3:{s:4:\"name\";s:9:\"marketing\";s:4:\"slug\";s:9:\"marketing\";s:5:\"count\";i:348;}s:4:\"chat\";a:3:{s:4:\"name\";s:4:\"chat\";s:4:\"slug\";s:4:\"chat\";s:5:\"count\";i:347;}s:12:\"social-media\";a:3:{s:4:\"name\";s:12:\"social media\";s:4:\"slug\";s:12:\"social-media\";s:5:\"count\";i:346;}s:7:\"plugins\";a:3:{s:4:\"name\";s:7:\"plugins\";s:4:\"slug\";s:7:\"plugins\";s:5:\"count\";i:343;}s:15:\"payment-gateway\";a:3:{s:4:\"name\";s:15:\"payment gateway\";s:4:\"slug\";s:15:\"payment-gateway\";s:5:\"count\";i:341;}s:9:\"multisite\";a:3:{s:4:\"name\";s:9:\"multisite\";s:4:\"slug\";s:9:\"multisite\";s:5:\"count\";i:338;}s:4:\"code\";a:3:{s:4:\"name\";s:4:\"code\";s:4:\"slug\";s:4:\"code\";s:5:\"count\";i:337;}s:10:\"newsletter\";a:3:{s:4:\"name\";s:10:\"newsletter\";s:4:\"slug\";s:10:\"newsletter\";s:5:\"count\";i:337;}s:4:\"list\";a:3:{s:4:\"name\";s:4:\"list\";s:4:\"slug\";s:4:\"list\";s:5:\"count\";i:332;}s:3:\"url\";a:3:{s:4:\"name\";s:3:\"url\";s:4:\"slug\";s:3:\"url\";s:5:\"count\";i:331;}s:4:\"meta\";a:3:{s:4:\"name\";s:4:\"meta\";s:4:\"slug\";s:4:\"meta\";s:5:\"count\";i:329;}s:8:\"redirect\";a:3:{s:4:\"name\";s:8:\"redirect\";s:4:\"slug\";s:8:\"redirect\";s:5:\"count\";i:321;}s:5:\"forms\";a:3:{s:4:\"name\";s:5:\"forms\";s:4:\"slug\";s:5:\"forms\";s:5:\"count\";i:312;}s:6:\"simple\";a:3:{s:4:\"name\";s:6:\"simple\";s:4:\"slug\";s:6:\"simple\";s:5:\"count\";i:304;}s:16:\"custom-post-type\";a:3:{s:4:\"name\";s:16:\"custom post type\";s:4:\"slug\";s:16:\"custom-post-type\";s:5:\"count\";i:303;}s:11:\"advertising\";a:3:{s:4:\"name\";s:11:\"advertising\";s:4:\"slug\";s:11:\"advertising\";s:5:\"count\";i:303;}s:3:\"tag\";a:3:{s:4:\"name\";s:3:\"tag\";s:4:\"slug\";s:3:\"tag\";s:5:\"count\";i:300;}s:7:\"adsense\";a:3:{s:4:\"name\";s:7:\"adsense\";s:4:\"slug\";s:7:\"adsense\";s:5:\"count\";i:297;}s:4:\"html\";a:3:{s:4:\"name\";s:4:\"html\";s:4:\"slug\";s:4:\"html\";s:5:\"count\";i:295;}s:12:\"notification\";a:3:{s:4:\"name\";s:12:\"notification\";s:4:\"slug\";s:12:\"notification\";s:5:\"count\";i:294;}s:8:\"tracking\";a:3:{s:4:\"name\";s:8:\"tracking\";s:4:\"slug\";s:8:\"tracking\";s:5:\"count\";i:292;}s:6:\"author\";a:3:{s:4:\"name\";s:6:\"author\";s:4:\"slug\";s:6:\"author\";s:5:\"count\";i:290;}s:16:\"google-analytics\";a:3:{s:4:\"name\";s:16:\"google analytics\";s:4:\"slug\";s:16:\"google-analytics\";s:5:\"count\";i:290;}s:11:\"performance\";a:3:{s:4:\"name\";s:11:\"performance\";s:4:\"slug\";s:11:\"performance\";s:5:\"count\";i:290;}s:8:\"lightbox\";a:3:{s:4:\"name\";s:8:\"lightbox\";s:4:\"slug\";s:8:\"lightbox\";s:5:\"count\";i:285;}}', 'no'),
(429, 'woocommerce_version', '3.3.5', 'yes'),
(430, 'woocommerce_db_version', '3.3.5', 'yes'),
(431, 'category_children', 'a:0:{}', 'yes'),
(443, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pl_PL/wordpress-4.9.5.zip\";s:6:\"locale\";s:5:\"pl_PL\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pl_PL/wordpress-4.9.5.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.5\";s:7:\"version\";s:5:\"4.9.5\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1525125090;s:15:\"version_checked\";s:5:\"4.9.5\";s:12:\"translations\";a:0:{}}', 'no'),
(446, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1525125091;s:7:\"checked\";a:4:{s:30:\"advanced-custom-fields/acf.php\";s:6:\"4.4.12\";s:19:\"akismet/akismet.php\";s:5:\"4.0.3\";s:9:\"hello.php\";s:3:\"1.7\";s:27:\"woocommerce/woocommerce.php\";s:5:\"3.3.5\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:1:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"woocommerce\";s:8:\"language\";s:5:\"pl_PL\";s:7:\"version\";s:5:\"3.3.5\";s:7:\"updated\";s:19:\"2018-04-23 11:11:19\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/plugin/woocommerce/3.3.5/pl_PL.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:4:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:6:\"4.4.12\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.4.4.12.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.0.3\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.0.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"3.3.5\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.3.3.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=1440831\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=1440831\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=1629184\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=1629184\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(447, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1525125091;s:7:\"checked\";a:5:{s:10:\"storefront\";s:5:\"2.2.8\";s:13:\"twentyfifteen\";s:3:\"1.9\";s:15:\"twentyseventeen\";s:3:\"1.5\";s:19:\"twentysixteen-child\";s:3:\"1.4\";s:13:\"twentysixteen\";s:3:\"1.4\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:1:{i:0;a:7:{s:4:\"type\";s:5:\"theme\";s:4:\"slug\";s:10:\"storefront\";s:8:\"language\";s:5:\"pl_PL\";s:7:\"version\";s:5:\"2.2.8\";s:7:\"updated\";s:19:\"2018-01-23 10:36:21\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/translation/theme/storefront/2.2.8/pl_PL.zip\";s:10:\"autoupdate\";b:1;}}}', 'no'),
(458, '_site_transient_timeout_community-events-4501c091b0366d76ea3218b6cfdd8097', '1525169839', 'no'),
(459, '_site_transient_community-events-4501c091b0366d76ea3218b6cfdd8097', 'a:2:{s:8:\"location\";a:1:{s:2:\"ip\";s:2:\"::\";}s:6:\"events\";a:2:{i:0;a:7:{s:4:\"type\";s:8:\"wordcamp\";s:5:\"title\";s:16:\"WordCamp Košice\";s:3:\"url\";s:32:\"https://2018.kosice.wordcamp.org\";s:6:\"meetup\";N;s:10:\"meetup_url\";N;s:4:\"date\";s:19:\"2018-05-19 00:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:17:\"Košice, Slovakia\";s:7:\"country\";s:2:\"SK\";s:8:\"latitude\";d:48.7316221;s:9:\"longitude\";d:21.2443364;}}i:1;a:7:{s:4:\"type\";s:8:\"wordcamp\";s:5:\"title\";s:16:\"WordCamp Poznań\";s:3:\"url\";s:32:\"https://2018.poznan.wordcamp.org\";s:6:\"meetup\";N;s:10:\"meetup_url\";N;s:4:\"date\";s:19:\"2018-07-06 00:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:15:\"Poznań, Poland\";s:7:\"country\";s:2:\"PL\";s:8:\"latitude\";d:52.4153966;s:9:\"longitude\";d:16.9250321;}}}}', 'no'),
(460, '_transient_timeout_feed_a421d6f32723068ab074a40017a9e1f9', '1525169839', 'no');
INSERT INTO `rs_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(461, '_transient_feed_a421d6f32723068ab074a40017a9e1f9', 'a:4:{s:5:\"child\";a:1:{s:0:\"\";a:1:{s:3:\"rss\";a:1:{i:0;a:6:{s:4:\"data\";s:3:\"\n\n\n\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:7:\"version\";s:3:\"2.0\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:1:{s:0:\"\";a:1:{s:7:\"channel\";a:1:{i:0;a:6:{s:4:\"data\";s:49:\"\n	\n	\n	\n	\n	\n	\n	\n	\n	\n	\n		\n		\n		\n		\n		\n		\n		\n		\n		\n	\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:3:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:6:\"Polska\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:24:\"https://pl.wordpress.org\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:0:\"\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:13:\"lastBuildDate\";a:1:{i:0;a:5:{s:4:\"data\";s:34:\"\n	Thu, 29 Mar 2018 03:03:47 +0000	\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"language\";a:1:{i:0;a:5:{s:4:\"data\";s:5:\"pl-PL\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:9:\"generator\";a:1:{i:0;a:5:{s:4:\"data\";s:40:\"https://wordpress.org/?v=5.0-alpha-43041\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"item\";a:10:{i:0;a:6:{s:4:\"data\";s:42:\"\n		\n		\n		\n		\n		\n				\n\n		\n		\n				\n			\n		\n		\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:55:\"WordPress 4.9.4 – Wydanie poprawiające ważny błąd\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:84:\"https://pl.wordpress.org/2018/02/06/wordpress-4-9-4-wydanie-poprawiajace-wazny-blad/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:93:\"https://pl.wordpress.org/2018/02/06/wordpress-4-9-4-wydanie-poprawiajace-wazny-blad/#comments\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Tue, 06 Feb 2018 18:25:48 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:7:\"Wydanie\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:32:\"https://pl.wordpress.org/?p=1539\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:489:\"WordPress 4.9.4 jest już dostępny. To wydanie rozwiązuje istotny problem wprowadzony w wersji 4.9.3, który powodował, że strony obsługujące automatyczne aktualizacje nie będą mogły ich przeprowadzić. Wymagane jest ręczne zaktualizowanie WordPressa do wersji 4.9.4 przez Ciebie lub Twojego dostawcę hostingu. Cztery lata temu, w wydaniu 3.7 &#8222;Basie&#8221; WordPressa wprowadziliśmy możliwość automatycznej aktualizacji systemu, dzięki czemu strony stały [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:37:\"Krzysztof Trynkiewicz (Sukces Strony)\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:2631:\"<p>WordPress 4.9.4 jest już dostępny.</p>\n<p>To wydanie rozwiązuje istotny problem wprowadzony w wersji 4.9.3, który powodował, że strony obsługujące automatyczne aktualizacje nie będą mogły ich przeprowadzić. <strong><span style=\"text-decoration: underline\">Wymagane jest ręczne zaktualizowanie WordPressa</span> do wersji 4.9.4 przez Ciebie lub Twojego dostawcę hostingu.</strong></p>\n<p><span id=\"more-1539\"></span></p>\n<p>Cztery lata temu, w wydaniu <a href=\"https://wordpress.org/news/2013/10/basie/\">3.7 &#8222;Basie&#8221; WordPressa</a> wprowadziliśmy możliwość automatycznej aktualizacji systemu, dzięki czemu strony stały się bezpieczniejsze i mniej awaryjne &#8211; nawet, jeśli samodzielnie nie miałeś możliwości ich aktualizować. Przez te 4 lata mechanizm ten pomógł milionom witryn, aktualizując je do bieżących wersji i naprawiając bardzo sporadyczne usterki. Niestety we wczorajszym <a href=\"https://pl.wordpress.org/2018/02/06/wordpress-4-9-3-wydanie-poprawiajace-bledy/\">wydaniu 4.9.3</a> znalazł się ważny błąd, który został wykryty dopiero po upublicznieniu nowej wersji systemu. Błąd ten powoduje, że WordPress nie zaktualizuje się automatycznie do wersji 4.9.4, co oznacza, że aktualizację taką trzeba przeprowadzić ręcznie z poziomu panelu administracyjnego lub z pomocą dostawcy serwera hostingowego.</p>\n<p><strong>Aby przeprowadzić ręczną aktualizację, udaj się do sekcji Kokpit → Aktualizacje panelu administracyjnego i po prostu kliknij przycisk „Zaktualizuj teraz”. Dopóki tego nie wykonasz, WordPress <span style=\"text-decoration: underline\">nie będzie aktualizował się automatycznie do kolejnych wersji</span>. Zalecamy, by zrobić to natychmiast.</strong></p>\n<p>Dostawcy hostingu, którzy aktualizują automatycznie WordPressa w imieniu swoich klientów, mogą przystąpić do aktualizacji w standardowym trybie. Będziemy ściśle współpracować z innymi dostawcami hostingu, by jak najwięcej ich klientów otrzymało tą aktualizację.</p>\n<p><a href=\"https://make.wordpress.org/core/2018/02/06/wordpress-4-9-4-release-the-technical-details/\">Więcej szczegółów technicznych opublikowaliśmy na naszym blogu o tworzeniu rdzenia WordPressa</a>. Pełna lista zmian dostępna jest w <a href=\"https://core.trac.wordpress.org/query?status=closed&amp;milestone=4.9.4&amp;group=component\">spisie zamkniętych wątków</a>.</p>\n<p><a href=\"https://pl.wordpress.org/\">Pobierz WordPressa 4.9.4</a> lub udaj się do sekcji Kokpit → Aktualizacje panelu administracyjnego i po prostu kliknij przycisk „Zaktualizuj teraz”.</p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:89:\"https://pl.wordpress.org/2018/02/06/wordpress-4-9-4-wydanie-poprawiajace-wazny-blad/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"4\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:1;a:6:{s:4:\"data\";s:42:\"\n		\n		\n		\n		\n		\n				\n\n		\n		\n				\n			\n		\n		\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:49:\"WordPress 4.9.3 – Wydanie poprawiające błędy\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:79:\"https://pl.wordpress.org/2018/02/06/wordpress-4-9-3-wydanie-poprawiajace-bledy/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:88:\"https://pl.wordpress.org/2018/02/06/wordpress-4-9-3-wydanie-poprawiajace-bledy/#comments\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Tue, 06 Feb 2018 10:50:35 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:7:\"Wydanie\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:32:\"https://pl.wordpress.org/?p=1536\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:450:\"WordPress 4.9.3 jest już dostępny. To wydanie zawiera poprawki 34 błędów obecnych w wersji 4.9 WordPressa, w tym dotyczących zestawów zmian na ekranie Personalizacji, widgetów, edytora wizualnego i kompatybilności z PHP 7.2.  Spis wszystkich zmian znajdziesz w liście zamkniętych wątków oraz liście przeprowadzonych zmian. Pobierz WordPressa 4.9.3 lub udaj się do sekcji Kokpit → Aktualizacje panelu administracyjnego i [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:37:\"Krzysztof Trynkiewicz (Sukces Strony)\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:3596:\"<p>WordPress 4.9.3 jest już dostępny.</p>\n<p>To wydanie zawiera poprawki 34 błędów obecnych w wersji 4.9 WordPressa, w tym dotyczących zestawów zmian na ekranie Personalizacji, widgetów, edytora wizualnego i kompatybilności z PHP 7.2.  Spis wszystkich zmian znajdziesz<span style=\"line-height: 1.5\"> w <a href=\"https://core.trac.wordpress.org/query?status=closed&amp;milestone=4.9.3&amp;group=component\">liście zamkniętych wątków</a> oraz <a href=\"https://core.trac.wordpress.org/log/branches/4.9?rev=42630&amp;stop_rev=42521\">liście przeprowadzonych zmian</a>.</span></p>\n<p><span id=\"more-1536\"></span></p>\n<p><a href=\"https://pl.wordpress.org/\">Pobierz WordPressa 4.9.3</a> lub udaj się do sekcji Kokpit → Aktualizacje panelu administracyjnego i po prostu kliknij przycisk „Zaktualizuj teraz”. Witryny, które obsługują automatyczne aktualizacje, rozpoczęły już ich przeprowadzanie.</p>\n<p>Dziękujemy wszystkim, którzy przyczynili się do powstania wersji 4.9.3:</p>\n<p><a href=\"https://profiles.wordpress.org/jorbin/\">Aaron Jorbin</a>, <a href=\"https://profiles.wordpress.org/abdullahramzan/\">abdullahramzan</a>, <a href=\"https://profiles.wordpress.org/adamsilverstein/\">Adam Silverstein</a>, <a href=\"https://profiles.wordpress.org/afercia/\">Andrea Fercia</a>, <a href=\"https://profiles.wordpress.org/andreiglingeanu/\">andreiglingeanu</a>, <a href=\"https://profiles.wordpress.org/azaozz/\">Andrew Ozz</a>, <a href=\"https://profiles.wordpress.org/bpayton/\">Brandon Payton</a>, <a href=\"https://profiles.wordpress.org/chetan200891/\">Chetan Prajapati</a>, <a href=\"https://profiles.wordpress.org/coleh/\">coleh</a>, <a href=\"https://profiles.wordpress.org/darko-a7/\">Darko A7</a>, <a href=\"https://profiles.wordpress.org/desertsnowman/\">David Cramer</a>, <a href=\"https://profiles.wordpress.org/dlh/\">David Herrera</a>, <a href=\"https://profiles.wordpress.org/dd32/\">Dion Hulse</a>, <a href=\"https://profiles.wordpress.org/flixos90/\">Felix Arntz</a>, <a href=\"https://profiles.wordpress.org/frank-klein/\">Frank Klein</a>, <a href=\"https://profiles.wordpress.org/pento/\">Gary Pendergast</a>, <a href=\"https://profiles.wordpress.org/audrasjb/\">Jb Audras</a>, <a href=\"https://profiles.wordpress.org/jbpaul17/\">Jeffrey Paul</a>, <a href=\"https://profiles.wordpress.org/lizkarkoski/\">lizkarkoski</a>, <a href=\"https://profiles.wordpress.org/clorith/\">Marius L. J.</a>, <a href=\"https://profiles.wordpress.org/mattyrob/\">mattyrob</a>, <a href=\"https://profiles.wordpress.org/munyagu/\">munyagu</a>, <a href=\"https://profiles.wordpress.org/ndavison/\">ndavison</a>, <a href=\"https://profiles.wordpress.org/nickmomrik/\">Nick Momrik</a>, <a href=\"https://profiles.wordpress.org/peterwilsoncc/\">Peter Wilson</a>, <a href=\"https://profiles.wordpress.org/rachelbaker/\">Rachel Baker</a>, <a href=\"https://profiles.wordpress.org/rishishah/\">rishishah</a>, <a href=\"https://profiles.wordpress.org/othellobloke/\">Ryan Paul</a>, <a href=\"https://profiles.wordpress.org/sasiddiqui/\">Sami Ahmed Siddiqui</a>, <a href=\"https://profiles.wordpress.org/sayedwp/\">Sayed Taqui</a>, <a href=\"https://profiles.wordpress.org/seanchayes/\">Sean Hayes</a>, <a href=\"https://profiles.wordpress.org/sergeybiryukov/\">Sergey Biryukov</a>, <a href=\"https://profiles.wordpress.org/shooper/\">Shawn Hooper</a>, <a href=\"https://profiles.wordpress.org/netweb/\">Stephen Edgar</a>, <a href=\"https://profiles.wordpress.org/manikmist09/\">Sultan Nasir Uddin</a>, <a href=\"https://profiles.wordpress.org/tigertech/\">tigertech</a> oraz <a href=\"https://profiles.wordpress.org/westonruter/\">Weston Ruter</a>.</p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:84:\"https://pl.wordpress.org/2018/02/06/wordpress-4-9-3-wydanie-poprawiajace-bledy/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"2\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:2;a:6:{s:4:\"data\";s:42:\"\n		\n		\n		\n		\n		\n				\n\n		\n		\n				\n			\n		\n		\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:67:\"WordPress 4.9.2 – Wydanie poprawiające bezpieczeństwo i błędy\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:96:\"https://pl.wordpress.org/2018/01/17/wordpress-4-9-2-wydanie-poprawiajace-bezpieczenstwo-i-bledy/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:105:\"https://pl.wordpress.org/2018/01/17/wordpress-4-9-2-wydanie-poprawiajace-bezpieczenstwo-i-bledy/#comments\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Wed, 17 Jan 2018 11:06:38 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:7:\"Wydanie\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:32:\"https://pl.wordpress.org/?p=1512\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:467:\"WordPress 4.9.2 jest już dostępny. To wydanie poprawia bezpieczeństwo we wszystkich poprzednich wersjach systemu. Zalecane jest natychmiastowe przeprowadzenie aktualizacji. Wykryto podatność na atak XSS w plikach Flash biblioteki MediaElement, która jest dołączana do WordPressa w celu zapewnienia kompatybilności ze starszym oprogramowaniem. Ponieważ pliki te nie są już potrzebne w zdecydowanej większości przypadków, zostały one usunięte [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:37:\"Krzysztof Trynkiewicz (Sukces Strony)\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:4168:\"<p>WordPress 4.9.2 jest już dostępny. <strong>To wydanie poprawia bezpieczeństwo</strong> we wszystkich poprzednich wersjach systemu. Zalecane jest natychmiastowe przeprowadzenie aktualizacji.</p>\n<p><span id=\"more-1512\"></span></p>\n<p>Wykryto podatność na atak XSS w plikach Flash biblioteki MediaElement, która jest dołączana do WordPressa w celu zapewnienia kompatybilności ze starszym oprogramowaniem. Ponieważ pliki te nie są już potrzebne w zdecydowanej większości przypadków, zostały one usunięte z WordPressa.</p>\n<p>Biblioteka MediaElement została już udostępniona w nowej wersji, która naprawa problem. W repozytorium wtyczek WordPressa znajduje się nowa <a href=\"https://wordpress.org/plugins/mediaelement-flash-fallbacks/\">wtyczka, przywracająca obsługę biblioteki MediaElement z poprawionymi plikami</a>.</p>\n<p>Dziękujemy zgłaszającym (<a href=\"https://opnsec.com\">Enguerran Gillier</a> i <a href=\"https://widiz.com/\">Widiz</a>) za <a href=\"https://make.wordpress.org/core/handbook/reporting-security-vulnerabilities/\">odpowiedzialne ujawnienie</a> problemów z bezpieczeństwem.</p>\n<p>W WordPressie 4.9.2 rozwiązano także 21 innych problemów, w tym:</p>\n<ul>\n<li>naprawiono błędy JavaScript, które uniemożliwiały zapisywanie wpisów w Firefoxie,</li>\n<li>przywrócono wcześniejsze, niezależne od taksonomii działanie <code>get_category_link()</code> i <code>category_description()</code>,</li>\n<li>od teraz zmiana motywu spowoduje podjęcie próby przywrócenia poprzednich przypisań widgetów &#8211; nawet, jeśli w ostatnio używanym motywie nie były dostępne żadne obszary na widgety.</li>\n</ul>\n<p>W dokumentacji Codex znajdziesz <a href=\"https://codex.wordpress.org/Version_4.9.2\">więcej informacji na temat wszystkich rozwiązanych problemów w wersji 4.9.2</a>.</p>\n<p><a href=\"https://pl.wordpress.org/\">Pobierz WordPressa 4.9.2</a> lub udaj się do sekcji Kokpit → Aktualizacje panelu administracyjnego i po prostu kliknij przycisk „Zaktualizuj teraz”. Witryny, które obsługują automatyczne aktualizacje, rozpoczęły już ich przeprowadzanie.</p>\n<p>Dziękujemy wszystkim, którzy przyczynili się do powstania wersji 4.9.2:</p>\n<p><a href=\"https://profiles.wordpress.org/0x6f0/\">0x6f0</a>, <a href=\"https://profiles.wordpress.org/jorbin/\">Aaron Jorbin</a>, <a href=\"https://profiles.wordpress.org/afercia/\">Andrea Fercia</a>, <a href=\"https://profiles.wordpress.org/aduth/\">Andrew Duthie</a>, <a href=\"https://profiles.wordpress.org/azaozz/\">Andrew Ozz</a>, <a href=\"https://profiles.wordpress.org/blobfolio/\">Blobfolio</a>, <a href=\"https://profiles.wordpress.org/boonebgorges/\">Boone Gorges</a>, <a href=\"https://profiles.wordpress.org/icaleb/\">Caleb Burks</a>, <a href=\"https://profiles.wordpress.org/poena/\">Carolina Nymark</a>, <a href=\"https://profiles.wordpress.org/chasewg/\">chasewg</a>, <a href=\"https://profiles.wordpress.org/chetan200891/\">Chetan Prajapati</a>, <a href=\"https://profiles.wordpress.org/dd32/\">Dion Hulse</a>, <a href=\"https://profiles.wordpress.org/hardik-amipara/\">Hardik Amipara</a>, <a href=\"https://profiles.wordpress.org/ionvv/\">ionvv</a>, <a href=\"https://profiles.wordpress.org/jaswrks/\">Jason Caldwell</a>, <a href=\"https://profiles.wordpress.org/jbpaul17/\">Jeffrey Paul</a>, <a href=\"https://profiles.wordpress.org/jeremyfelt/\">Jeremy Felt</a>, <a href=\"https://profiles.wordpress.org/joemcgill/\">Joe McGill</a>, <a href=\"https://profiles.wordpress.org/johnschulz/\">johnschulz</a>, <a href=\"https://profiles.wordpress.org/juiiee8487/\">Juhi Patel</a>, <a href=\"https://profiles.wordpress.org/obenland/\">Konstantin Obenland</a>, <a href=\"https://profiles.wordpress.org/markjaquith/\">Mark Jaquith</a>, <a href=\"https://profiles.wordpress.org/rabmalin/\">Nilambar Sharma</a>, <a href=\"https://profiles.wordpress.org/peterwilsoncc/\">Peter Wilson</a>, <a href=\"https://profiles.wordpress.org/rachelbaker/\">Rachel Baker</a>, <a href=\"https://profiles.wordpress.org/rinkuyadav999/\">Rinku Y</a>, <a href=\"https://profiles.wordpress.org/sergeybiryukov/\">Sergey Biryukov</a> oraz <a href=\"https://profiles.wordpress.org/westonruter/\">Weston Ruter</a>.</p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:101:\"https://pl.wordpress.org/2018/01/17/wordpress-4-9-2-wydanie-poprawiajace-bezpieczenstwo-i-bledy/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"1\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:3;a:6:{s:4:\"data\";s:45:\"\n		\n		\n		\n		\n		\n				\n		\n\n		\n		\n				\n			\n		\n		\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:57:\"WordPress 4.8.3 – Wydanie poprawiające bezpieczeństwo\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:88:\"https://pl.wordpress.org/2017/10/31/wordpress-4-8-3-wydanie-poprawiajace-bezpieczenstwo/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:97:\"https://pl.wordpress.org/2017/10/31/wordpress-4-8-3-wydanie-poprawiajace-bezpieczenstwo/#comments\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Tue, 31 Oct 2017 11:26:39 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:2:{i:0;a:5:{s:4:\"data\";s:15:\"Bezpieczeństwo\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}i:1;a:5:{s:4:\"data\";s:7:\"Wydanie\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:32:\"https://pl.wordpress.org/?p=1289\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:475:\"WordPress 4.8.3 jest już dostępny. To wydanie poprawia bezpieczeństwo we wszystkich poprzednich wersjach systemu. Zalecane jest natychmiastowe przeprowadzenie aktualizacji. WordPress w wersji 4.8.2 i wcześniejszych jest podatny na problem z $wpdb-&#62;prepare(), który może prowadzić do tworzenia nieoczekiwanych i niebezpiecznych zapytań, umożliwiających potencjalny atak metodą SQL injection (SQLi). Rdzeń systemu nie jest bezpośrednio podatny na ten [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:37:\"Krzysztof Trynkiewicz (Sukces Strony)\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:1522:\"<p>WordPress 4.8.3 jest już dostępny. <strong>To wydanie poprawia bezpieczeństwo</strong> we wszystkich poprzednich wersjach systemu. Zalecane jest natychmiastowe przeprowadzenie aktualizacji.</p>\n<p><span id=\"more-1289\"></span></p>\n<p>WordPress w wersji 4.8.2 i wcześniejszych jest podatny na problem z <code>$wpdb-&gt;prepare()</code>, który może prowadzić do tworzenia nieoczekiwanych i niebezpiecznych zapytań, umożliwiających potencjalny atak metodą SQL injection (SQLi). Rdzeń systemu nie jest bezpośrednio podatny na ten problem, ale dodaliśmy dodatkowe zabezpieczenia, by wtyczki i motywy nie mogły go przypadkowo powodować. Problem zgłosił <a href=\"https://twitter.com/ircmaxell\">Anthony Ferrara</a>.</p>\n<p>To wydanie WordPressa zmienia zachowanie funkcji <code>esc_sql()</code>. Różnica nie będzie istotna dla większości programistów. Zachęcamy do <a href=\"https://make.wordpress.org/core/2017/10/31/changed-behaviour-of-esc_sql-in-wordpress-4-8-3/\">przeczytania informacji dla deweloperów</a>.</p>\n<p>Dziękujemy zgłaszającemu za <a href=\"https://make.wordpress.org/core/handbook/testing/reporting-security-vulnerabilities/\">odpowiedzialne ujawnienie</a> problemu z bezpieczeństwem.</p>\n<p><a href=\"https://pl.wordpress.org/\">Pobierz WordPressa 4.8.3</a> lub udaj się do sekcji Kokpit → Aktualizacje panelu administracyjnego i po prostu kliknij przycisk „Zaktualizuj teraz”. Witryny, które obsługują automatyczne aktualizacje, rozpoczęły już ich przeprowadzanie.</p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:93:\"https://pl.wordpress.org/2017/10/31/wordpress-4-8-3-wydanie-poprawiajace-bezpieczenstwo/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"2\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:4;a:6:{s:4:\"data\";s:42:\"\n		\n		\n		\n		\n		\n				\n\n		\n		\n				\n			\n		\n		\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:20:\"WordPress 4.9 Beta 2\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:57:\"https://pl.wordpress.org/2017/10/12/wordpress-4-9-beta-2/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:66:\"https://pl.wordpress.org/2017/10/12/wordpress-4-9-beta-2/#comments\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Thu, 12 Oct 2017 07:03:53 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:14:\"Wersje testowe\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:32:\"https://pl.wordpress.org/?p=1235\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:433:\"WordPress 4.9 Beta 2 jest już dostępny! Oprogramowanie jest wciąż w fazie tworzenia, dlatego nie zalecamy jego używania w witrynie produkcyjnej. Zastanów się nad środowiskiem testowym i tam sprawdzaj do woli. Aby przetestować WordPress 4.9, wypróbuj wtyczkę WordPress Beta Tester (regularnie aktualizuje one do  &#8222;krawych nocnych wydań&#8221;) albo pobierz wersję beta (zip). Więcej informacji na temat [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:15:\"Marcin Pietrzak\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:1781:\"<p>WordPress 4.9 Beta 2 jest już dostępny!</p>\n<p><strong>Oprogramowanie jest wciąż w fazie tworzenia</strong>, dlatego nie zalecamy jego używania w witrynie produkcyjnej. Zastanów się nad środowiskiem testowym i tam sprawdzaj do woli. Aby przetestować WordPress 4.9, wypróbuj wtyczkę <a href=\"https://pl.wordpress.org/plugins/wordpress-beta-tester/\">WordPress Beta Tester</a> (regularnie aktualizuje one do  &#8222;krawych nocnych wydań&#8221;) albo <a href=\"https://wordpress.org/wordpress-4.9-beta2.zip\">pobierz wersję beta</a> (zip).</p>\n<p>Więcej informacji na temat tego, co nowego w wersji 4.9, znajdziesz w blogu we wpisie <a href=\"https://wordpress.org/news/2017/10/wordpress-4-9-beta-1/\">Beta 1</a> (po angielsku). Od tego czasu dokonaliśmy <a href=\"https://core.trac.wordpress.org/log/trunk/?action=stop_on_copy&amp;mode=stop_on_copy&amp;rev=41846&amp;stop_rev=41777&amp;limit=100&amp;sfp_email=&amp;sfph_mail=\">70 zmian</a> w wersji Beta 2.</p>\n<p><a href=\"https://translate.wordpress.org/locale/pl/default/wp/dev\">Pomóż nam przetłumaczyć WordPress na język polski</a>!</p>\n<p><strong>Jeśli widzisz błąd</strong>, napisz wiadomość na forum <a href=\"https://wordpress.org/support/forum/alphabeta\">Alpha/Beta</a>. Jeżeli coś nie działa, to chcielibyśm się tego dowiedzieć właśnie do ciebie! Jeśli czujesz się na siłach, żeby napisać raport zawierający opis jak powtórzyć błąd to  skorzystaj z serwisu <a href=\"https://make.wordpress.org/core/reports/\">WordPress Trac</a>. Na nim też znajdziesz <a href=\"https://core.trac.wordpress.org/tickets/major\">listę znanych błędów</a>.</p>\n<p><em>Przetestujmy razem:</em><br />\n<em> edytowanie kodu, przełączniki tematyczne,</em><br />\n<em> widgety, harmonogramowanie.</em></p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:62:\"https://pl.wordpress.org/2017/10/12/wordpress-4-9-beta-2/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"3\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:5;a:6:{s:4:\"data\";s:45:\"\n		\n		\n		\n		\n		\n				\n		\n\n		\n		\n				\n			\n		\n		\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:67:\"WordPress 4.8.2 – Wydanie poprawiające bezpieczeństwo i błędy\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:96:\"https://pl.wordpress.org/2017/09/22/wordpress-4-8-2-wydanie-poprawiajace-bezpieczenstwo-i-bledy/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:104:\"https://pl.wordpress.org/2017/09/22/wordpress-4-8-2-wydanie-poprawiajace-bezpieczenstwo-i-bledy/#respond\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Fri, 22 Sep 2017 11:37:20 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:2:{i:0;a:5:{s:4:\"data\";s:15:\"Bezpieczeństwo\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}i:1;a:5:{s:4:\"data\";s:7:\"Wydanie\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:32:\"https://pl.wordpress.org/?p=1213\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:476:\"WordPress 4.8.2 jest już dostępny. Wersja poprawia bezpieczeństwo wszystkich poprzednich wersji i zdecydowanie zachęcamy do natychmiastowej aktualizacji witryn. WordPress w wersji 4.8.1 i starsze mają następujące problemy z bezpieczeństwem: $wpdb-&#62; prepare() mogło tworzyć nieoczekiwane i niebezpieczne zapytania prowadzące do potencjalnego wstrzyknięcia kod SQL (SQLi). Core WordPressa nie jest bezpośrednio narażony na ten problem, ale poprawiono [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:15:\"Marcin Pietrzak\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:3051:\"<p>WordPress 4.8.2 jest już dostępny. Wersja <strong>poprawia bezpieczeństwo</strong> wszystkich poprzednich wersji i zdecydowanie zachęcamy do natychmiastowej aktualizacji witryn.</p>\n<p>WordPress w wersji 4.8.1 i starsze mają następujące problemy z bezpieczeństwem:</p>\n<ol>\n<li><strong>$wpdb-&gt; prepare()</strong> mogło tworzyć nieoczekiwane i niebezpieczne zapytania prowadzące do potencjalnego wstrzyknięcia kod SQL (SQLi). Core WordPressa nie jest bezpośrednio narażony na ten problem, ale poprawiono proces tworzenia zapytań aby zapobiegać podatności we wtyczkach i motywach. Zgłoszone przez <a href=\"https://hackerone.com/slavco\">Slavco</a></li>\n<li>Wykryto podatność na cross-site scripting (XSS) w używaniu oEmbed. Zgłoszone przez xknown zespołu ds. Zabezpieczeń WordPress.</li>\n<li>W edytorze wizualnym wykryto lukę w zabezpieczeniach skryptów przed cross-site scripting (XSS) Zgłoszone przez <a href=\"https://twitter.com/brutelogic\">Rodolfo Assis (@brutelogic)</a> Sucuri Security.</li>\n<li>W kodzie do rozpakowywania wykryto lukę w zabezpieczeniach ścieżki do pliku. Zgłoszone przez <a href=\"https://hackerone.com/noxrnet\">Alex Chapman (noxrnet)</a>.</li>\n<li>W edytorze wtyczek wykryto luka w zabezpieczeniach cross-site scripting (XSS). Zgłoszone przez 陈瑞琦 (Chen Ruiqi).</li>\n<li>Na ekranie użytkownika i terminach edycji zostały wykryte otwarte przekierowanie. Zgłoszone przez <a href=\"https://hackerone.com/ysx\">Yasin Soliman (ysx)</a>.</li>\n<li>Odkryto lukę w zabezpieczeniach ścieżki na ekranie personalizacji witryny. Zgłoszone przez Weston Ruter z zespołu ds. Bezpieczeństwa WordPress.</li>\n<li>Wykryto podatność na cross-site scripting (XSS)  w nazwach szablonów. Zgłoszone przez <a href=\"https://hackerone.com/sikic\">Luka (sikic)</a>.</li>\n<li>Wykryto podatność na cross-site scripting (XSS) w linkach modalnych. Zgłoszone przez <a href=\"https://hackerone.com/qasuar\">Anas Roubi (qasuar)</a>.</li>\n</ol>\n<p>Dziękujemy zgłaszającym za praktykowanie <a href=\"https://make.wordpress.org/core/handbook/testing/reporting-security-vulnerabilities/\">odpowiedzialnego ujawnienia (en)</a>.</p>\n<p>Oprócz powyższych kwestii związanych z bezpieczeństwem, WordPress 4.8.2 zawiera 6 poprawek utrzymaniowych serii 4.8. Więcej informacji można znaleźć w <a href=\"https://codex.wordpress.org/Version_4.8.2\">notce wydania wersji (en)</a> oraz na <a href=\"https://core.trac.wordpress.org/query?status=closed&amp;milestone=4.8.2&amp;group=component&amp;col=id&amp;col=summary&amp;col=component&amp;col=status&amp;col=owner&amp;col=type&amp;col=priority&amp;col=keywords&amp;order=priority\">liście zmian</a>.</p>\n<p><a href=\"https://pl.wordpress.org/wordpress-4.8.2-pl_PL.zip\">Pobierz WordPress 4.8.2</a> lub przejdź do Panel → Aktualizacje i kliknij przycisk &#8222;Aktualizuj teraz&#8221;. Strony obsługujące automatyczne aktualizacje w tle już zaczynają aktualizować się do wersji 4.8.2.</p>\n<p>Dziękujemy wszystkim, którzy przyczynili się do wydania 4.8.2.</p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:101:\"https://pl.wordpress.org/2017/09/22/wordpress-4-8-2-wydanie-poprawiajace-bezpieczenstwo-i-bledy/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"0\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:6;a:6:{s:4:\"data\";s:42:\"\n		\n		\n		\n		\n		\n				\n\n		\n		\n				\n			\n		\n		\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:51:\"Zrób jedną rzecz i zmień przyszłość WordCampa\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:60:\"https://pl.wordpress.org/2017/06/15/zrob-cos-dla-wordpressa/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:68:\"https://pl.wordpress.org/2017/06/15/zrob-cos-dla-wordpressa/#respond\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Thu, 15 Jun 2017 04:11:28 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:10:\"Wydarzenia\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"https://pl.wordpress.org/?p=977\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:435:\"Ósmy polski WordCamp już za nami. Polska społeczność rozrasta się z roku na rok, co bardzo cieszy, ale niejednokrotnie stanowi wyzwanie dla organizatorów spotkań. Chcemy lepiej odpowiadać na potrzeby polskich twórców i użytkowników WordPressa. Dlatego przygotowaliśmy krótką ankietę na temat WordCampa. Chcemy poznać Was lepiej, dowiedzieć się, kim jesteście i jakie macie oczekiwania względem kolejnych [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:18:\"Katarzyna Gajewska\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:815:\"<p>Ósmy polski WordCamp już za nami. Polska społeczność rozrasta się z roku na rok, co bardzo cieszy, ale niejednokrotnie stanowi wyzwanie dla organizatorów spotkań.</p>\n<p>Chcemy lepiej odpowiadać na potrzeby polskich twórców i użytkowników WordPressa. Dlatego przygotowaliśmy krótką ankietę na temat WordCampa. Chcemy poznać Was lepiej, dowiedzieć się, kim jesteście i jakie macie oczekiwania względem kolejnych edycji WordCampa w Polsce. Wypełnienie ankiety nie zajmie Wam więcej niż 5 minut.</p>\n<p><a href=\"https://goo.gl/forms/VPY4xtO9KF6fuDnm1\" target=\"_blank\" rel=\"noopener\"><strong>Wypełnij ankietę na temat polskiej społeczności WordPressowej</strong></a>.</p>\n<p>Dziękujemy Wam za wypełnienie ankiety!</p>\n<p><em>Organizatorzy polskich WordUpów i WordCamp Polska</em></p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:65:\"https://pl.wordpress.org/2017/06/15/zrob-cos-dla-wordpressa/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"0\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:7;a:6:{s:4:\"data\";s:42:\"\n		\n		\n		\n		\n		\n				\n\n		\n		\n				\n			\n		\n		\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:67:\"WordPress 4.7.5 – Wydanie poprawiające bezpieczeństwo i błędy\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:96:\"https://pl.wordpress.org/2017/05/17/wordpress-4-7-5-wydanie-poprawiajace-bezpieczenstwo-i-bledy/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:105:\"https://pl.wordpress.org/2017/05/17/wordpress-4-7-5-wydanie-poprawiajace-bezpieczenstwo-i-bledy/#comments\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Wed, 17 May 2017 00:14:32 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:7:\"Wydanie\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"https://pl.wordpress.org/?p=907\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:490:\"WordPress 4.7.5 jest już dostępny. To wydanie poprawia bezpieczeństwo we wszystkich poprzednich wersjach systemu. Zalecane jest natychmiastowe przeprowadzenie aktualizacji. WordPress w wersji 4.7.4 i wcześniejszych posiada sześć problemów z bezpieczeństwem: Niewystarczająca weryfikacja przekierowań w klasie HTTP. Zgłosił to Ronni Skansing. Niewłaściwa obsługa wartości danych meta wpisów w API XML-RPC. Zgłosił to Sam Thomas. Brak sprawdzania uprawnień dla [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:37:\"Krzysztof Trynkiewicz (Sukces Strony)\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:2591:\"<p>WordPress 4.7.5 jest już dostępny. <strong>To wydanie poprawia bezpieczeństwo</strong> we wszystkich poprzednich wersjach systemu. Zalecane jest natychmiastowe przeprowadzenie aktualizacji.</p>\n<p><span id=\"more-907\"></span></p>\n<p>WordPress w wersji 4.7.4 i wcześniejszych posiada sześć problemów z bezpieczeństwem:</p>\n<ol>\n<li>Niewystarczająca weryfikacja przekierowań w klasie HTTP. Zgłosił to <a href=\"https://dk.linkedin.com/in/ronni-skansing-36143b65\">Ronni Skansing</a>.</li>\n<li>Niewłaściwa obsługa wartości danych meta wpisów w API XML-RPC. Zgłosił to <a href=\"https://hackerone.com/jazzy2fives\">Sam Thomas</a>.</li>\n<li>Brak sprawdzania uprawnień dla danych meta wpisów w API XML-RPC. Zgłosił to <a href=\"https://profiles.wordpress.org/vortfu\">Ben Bidner</a> z Zespołu Bezpieczeństwa WordPressa.</li>\n<li>Podatność typu Cross Site Request Forgery (CRSF) została wykryta w formularzu zapytania o dane dostępu do systemu plików. Zgłosił to <a href=\"https://twitter.com/yorickkoster\">Yorick Koster</a>.</li>\n<li>Wykryto podatność typu cross-site scripting (XSS). Objawia się podczas próby wgrania bardzo dużych plików. Zgłosił to <a href=\"https://dk.linkedin.com/in/ronni-skansing-36143b65\">Ronni Skansing</a>.</li>\n<li>Wykryto podatność typu cross-site scripting (XSS). Ma ona związek z mechanizmem Personalizacji witryn. Zgłosił to <a href=\"https://profiles.wordpress.org/westonruter\">Weston Ruter</a> z Zespołu Bezpieczeństwa WordPressa.</li>\n</ol>\n<p>Dziękujemy zgłaszającym za <a href=\"https://make.wordpress.org/core/handbook/reporting-security-vulnerabilities/\">odpowiedzialne ujawnienie</a> problemów z bezpieczeństwem.</p>\n<p>Oprócz powyższych problemów z bezpieczeństwem, WordPress 4.7.5 naprawia także 3 błędy z wersji 4.7. Więcej informacji można znaleźć w <a href=\"https://codex.wordpress.org/Version_4.7.5\">notatkach do wydania</a> oraz <a href=\"https://core.trac.wordpress.org/query?status=closed&amp;milestone=4.7.5&amp;group=component&amp;col=id&amp;col=summary&amp;col=component&amp;col=status&amp;col=owner&amp;col=type&amp;col=priority&amp;col=keywords&amp;order=priority\">liście przeprowadzonych zmian</a>.</p>\n<p><a href=\"https://pl.wordpress.org/\">Pobierz WordPressa 4.7.5</a> lub udaj się do sekcji Kokpit → Aktualizacje panelu administracyjnego i po prostu kliknij przycisk „Zaktualizuj teraz”. Witryny, które obsługują automatyczne aktualizacje, rozpoczęły już ich przeprowadzanie.</p>\n<p>Dziękujemy wszystkim, którzy przyczynili się do powstania wersji 4.7.5.</p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:101:\"https://pl.wordpress.org/2017/05/17/wordpress-4-7-5-wydanie-poprawiajace-bezpieczenstwo-i-bledy/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"8\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:8;a:6:{s:4:\"data\";s:42:\"\n		\n		\n		\n		\n		\n				\n\n		\n		\n				\n			\n		\n		\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:67:\"WordPress 4.7.4 – Wydanie poprawiające bezpieczeństwo i błędy\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:96:\"https://pl.wordpress.org/2017/04/20/wordpress-4-7-4-wydanie-poprawiajace-bezpieczenstwo-i-bledy/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:104:\"https://pl.wordpress.org/2017/04/20/wordpress-4-7-4-wydanie-poprawiajace-bezpieczenstwo-i-bledy/#respond\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Thu, 20 Apr 2017 18:42:55 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:7:\"Wydanie\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"https://pl.wordpress.org/?p=858\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:490:\"Po niemal 60 milionach pobrań WordPressa 4.7 z radością ogłaszamy dostępność wersji 4.7.4 &#8211; wydania poprawiającego błędy i bezpieczeństwo. Wydanie to zawiera 47 poprawek błędów oraz usprawnień. W szczególności naprawiony został brak kompatybilności między nadchodzącą wersją przeglądarki Chrome i edytorem wizualnym, jak również poprawiono braki w spójności obsługi mediów, a także wprowadzono dalsze ulepszenia API REST. Więcej informacji [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:37:\"Krzysztof Trynkiewicz (Sukces Strony)\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:4736:\"<p>Po niemal 60 milionach pobrań WordPressa 4.7 z radością ogłaszamy dostępność wersji 4.7.4 &#8211; <strong>wydania poprawiającego błędy i bezpieczeństwo.</strong></p>\n<p><span id=\"more-858\"></span></p>\n<p>Wydanie to zawiera 47 poprawek błędów oraz usprawnień. W szczególności naprawiony został brak kompatybilności między nadchodzącą wersją przeglądarki Chrome i edytorem wizualnym, jak również poprawiono braki w spójności obsługi mediów, a także wprowadzono dalsze ulepszenia API REST. Więcej informacji można znaleźć w <a href=\"https://codex.wordpress.org/Version_4.7.4\">notatkach do wydania</a> oraz <a href=\"https://core.trac.wordpress.org/log/branches/4.7?rev=40487&amp;stop_rev=40224\">liście przeprowadzonych zmian</a>.</p>\n<p><a href=\"https://pl.wordpress.org/\">Pobierz WordPressa 4.7.4</a> lub udaj się do sekcji <strong>Kokpit → Aktualizacje</strong> panelu administracyjnego i po prostu kliknij przycisk „Zaktualizuj teraz”. Witryny, które obsługują automatyczne aktualizacje, rozpoczęły już ich przeprowadzanie.</p>\n<p>Dziękujemy wszystkim, którzy przyczynili się do powstania wersji 4.7.4:<br />\n<a href=\"https://profiles.wordpress.org/jorbin/\">Aaron Jorbin</a>, <a href=\"https://profiles.wordpress.org/adamsilverstein/\">Adam Silverstein</a>, <a href=\"https://profiles.wordpress.org/afercia/\">Andrea Fercia</a>, <a href=\"https://profiles.wordpress.org/azaozz/\">Andrew Ozz</a>, <a href=\"https://profiles.wordpress.org/aussieguy123/\">aussieguy123</a>, <a href=\"https://profiles.wordpress.org/blobfolio/\">Blobfolio</a>, <a href=\"https://profiles.wordpress.org/boldwater/\">boldwater</a>, <a href=\"https://profiles.wordpress.org/boonebgorges/\">Boone Gorges</a>, <a href=\"https://profiles.wordpress.org/bor0/\">Boro Sitnikovski</a>, <a href=\"https://profiles.wordpress.org/chesio/\">chesio</a>, <a href=\"https://profiles.wordpress.org/curdin/\">Curdin Krummenacher</a>, <a href=\"https://profiles.wordpress.org/danielbachhuber/\">Daniel Bachhuber</a>, <a href=\"https://profiles.wordpress.org/nerrad/\">Darren Ethier (nerrad)</a>, <a href=\"https://profiles.wordpress.org/davidakennedy/\">David A. Kennedy</a>, <a href=\"https://profiles.wordpress.org/davidbenton/\">davidbenton</a>, <a href=\"https://profiles.wordpress.org/dlh/\">David Herrera</a>, <a href=\"https://profiles.wordpress.org/dd32/\">Dion Hulse</a>, <a href=\"https://profiles.wordpress.org/ocean90/\">Dominik Schilling (ocean90)</a>, <a href=\"https://profiles.wordpress.org/eclev91/\">eclev91</a>, <a href=\"https://profiles.wordpress.org/iseulde/\">Ella Van Dorpe</a>, <a href=\"https://profiles.wordpress.org/ghosttoast/\">Gustave F. Gerhardt</a>, <a href=\"https://profiles.wordpress.org/ig_communitysites/\">ig_communitysites</a>, <a href=\"https://profiles.wordpress.org/jnylen0/\">James Nylen</a>, <a href=\"https://profiles.wordpress.org/joedolson/\">Joe Dolson</a>, <a href=\"https://profiles.wordpress.org/johnbillion/\">John Blackbourn</a>, <a href=\"https://profiles.wordpress.org/karinedo/\">karinedo</a>, <a href=\"https://profiles.wordpress.org/lukasbesch/\">lukasbesch</a>, <a href=\"https://profiles.wordpress.org/maguiar/\">maguiar</a>, <a href=\"https://profiles.wordpress.org/matheusgimenez/\">MatheusGimenez</a>, <a href=\"https://profiles.wordpress.org/mboynes/\">Matthew Boynes</a>, <a href=\"https://profiles.wordpress.org/mattwiebe/\">Matt Wiebe</a>, <a href=\"https://profiles.wordpress.org/mayurk/\">Mayur Keshwani</a>, <a href=\"https://profiles.wordpress.org/melchoyce/\">Mel Choyce</a>, <a href=\"https://profiles.wordpress.org/celloexpressions/\">Nick Halsey</a>, <a href=\"https://profiles.wordpress.org/swissspidy/\">Pascal Birchler</a>, <a href=\"https://profiles.wordpress.org/peterwilsoncc/\">Peter Wilson</a>, <a href=\"https://profiles.wordpress.org/delawski/\">Piotr Delawski</a>, <a href=\"https://profiles.wordpress.org/pratikshrestha/\">Pratik Shrestha</a>, <a href=\"https://profiles.wordpress.org/programmin/\">programmin</a>, <a href=\"https://profiles.wordpress.org/rachelbaker/\">Rachel Baker</a>, <a href=\"https://profiles.wordpress.org/sagarkbhatt/\">sagarkbhatt</a>, <a href=\"https://profiles.wordpress.org/sagarprajapati/\">Sagar Prajapati</a>, <a href=\"https://profiles.wordpress.org/sboisvert/\">sboisvert</a>, <a href=\"https://profiles.wordpress.org/wonderboymusic/\">Scott Taylor</a>, <a href=\"https://profiles.wordpress.org/sergeybiryukov/\">Sergey Biryukov</a>, <a href=\"https://profiles.wordpress.org/netweb/\">Stephen Edgar</a>, <a href=\"https://profiles.wordpress.org/cybr/\">Sybre Waaijer</a>, <a href=\"https://profiles.wordpress.org/timmydcrawford/\">Timmy Crawford</a>, <a href=\"https://profiles.wordpress.org/vortfu/\">vortfu</a> oraz <a href=\"https://profiles.wordpress.org/westonruter/\">Weston Ruter</a>.</p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:101:\"https://pl.wordpress.org/2017/04/20/wordpress-4-7-4-wydanie-poprawiajace-bezpieczenstwo-i-bledy/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"0\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:9;a:6:{s:4:\"data\";s:42:\"\n		\n		\n		\n		\n		\n				\n\n		\n		\n				\n			\n		\n		\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:30:\"WordCamp Lublin 2017 zaprasza!\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:66:\"https://pl.wordpress.org/2017/03/22/wordcamp-lublin-2017-zaprasza/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:75:\"https://pl.wordpress.org/2017/03/22/wordcamp-lublin-2017-zaprasza/#comments\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Wed, 22 Mar 2017 13:27:00 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:10:\"Wydarzenia\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"https://pl.wordpress.org/?p=760\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:423:\"W dniach 2-4 czerwca br. odbędzie się kolejna edycja polskiego WordCampa &#8211; konferencji poświęconej WordPressowi. Tym razem programiści, koderzy, blogerzy i inne osoby korzystające na co dzień z WordPressa spotkają się w Lublinie. W ramach konferencji odbędą się: 2 czerwca: contributor day Contributor Day to specjalne spotkanie podczas którego każdy będzie miał możliwość dołączenia do [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:15:\"Marcin Pietrzak\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:1614:\"<p>W dniach 2-4 czerwca br. odbędzie się kolejna edycja polskiego WordCampa &#8211; konferencji poświęconej WordPressowi. Tym razem programiści, koderzy, blogerzy i inne osoby korzystające na co dzień z WordPressa spotkają się w Lublinie.</p>\n<p>W ramach konferencji odbędą się:</p>\n<h2>2 czerwca: contributor day</h2>\n<p><a href=\"https://2017.lublin.wordcamp.org/czym-jest-contributor-day/\">Contributor Day</a> to specjalne spotkanie podczas którego każdy będzie miał możliwość dołączenia do osób tworzących zarówno WordPressa jak i powiązanych z nim projektów. Oznacza to również, że będzie to świetna okazja do wspólnej pracy nad polskimi tłumaczeniami.</p>\n<h2>3-4 czerwca: prelekcje i warsztaty</h2>\n<p>Podczas prezentacji i warsztatów będzie można nabyć solidną porcję wiedzy od najlepszych ekspertów WordPressa z kraju i z zagranicy. Dodatkowo, uczestnicy będą mogli wziąć udział w specjalnie przygotowanej grze konferencyjnej oraz wieczornej imprezie integracyjnej.</p>\n<p>WordCamp organizowany jest przez społeczność WordPressa, w tym także przez Ciebie! Jeśli chciałbyś pomóc &#8211; wystąpić na scenie, poprowadzić warsztaty lub zostać sponsorem koniecznie daj znać:</p>\n<ul>\n<li><a href=\"https://2017.lublin.wordcamp.org/zostan-prelegentem/\">zgłoszenia prelegentów i osób prowadzących warsztaty</a></li>\n<li><a href=\"https://2017.lublin.wordcamp.org/zostan-sponsorem/\">zgłoszenia sponsorów</a></li>\n</ul>\n<p>Po więcej informacji zapraszamy na oficjalną stronę: <a href=\"https://2017.lublin.wordcamp.org/\">WordCamp Lublin 2017</a>.</p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:71:\"https://pl.wordpress.org/2017/03/22/wordcamp-lublin-2017-zaprasza/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"1\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}}}s:27:\"http://www.w3.org/2005/Atom\";a:1:{s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:0:\"\";s:7:\"attribs\";a:1:{s:0:\"\";a:3:{s:4:\"href\";s:30:\"https://pl.wordpress.org/feed/\";s:3:\"rel\";s:4:\"self\";s:4:\"type\";s:19:\"application/rss+xml\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:44:\"http://purl.org/rss/1.0/modules/syndication/\";a:2:{s:12:\"updatePeriod\";a:1:{i:0;a:5:{s:4:\"data\";s:9:\"\n	hourly	\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:15:\"updateFrequency\";a:1:{i:0;a:5:{s:4:\"data\";s:4:\"\n	1	\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}}}}}}}}s:4:\"type\";i:128;s:7:\"headers\";O:42:\"Requests_Utility_CaseInsensitiveDictionary\":1:{s:7:\"\0*\0data\";a:8:{s:6:\"server\";s:5:\"nginx\";s:4:\"date\";s:29:\"Mon, 30 Apr 2018 22:17:18 GMT\";s:12:\"content-type\";s:34:\"application/rss+xml; charset=UTF-8\";s:6:\"x-olaf\";s:3:\"⛄\";s:13:\"last-modified\";s:29:\"Thu, 29 Mar 2018 03:03:47 GMT\";s:4:\"link\";s:61:\"<https://pl.wordpress.org/wp-json/>; rel=\"https://api.w.org/\"\";s:15:\"x-frame-options\";s:10:\"SAMEORIGIN\";s:4:\"x-nc\";s:9:\"HIT ord 2\";}}s:5:\"build\";s:14:\"20180430093745\";}', 'no');
INSERT INTO `rs_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(462, '_transient_timeout_feed_mod_a421d6f32723068ab074a40017a9e1f9', '1525169839', 'no'),
(463, '_transient_feed_mod_a421d6f32723068ab074a40017a9e1f9', '1525126639', 'no'),
(464, '_transient_timeout_dash_v2_5ed7e1a5d79caaf375d35c33f6782653', '1525169839', 'no'),
(465, '_transient_dash_v2_5ed7e1a5d79caaf375d35c33f6782653', '<div class=\"rss-widget\"><ul><li><a class=\'rsswidget\' href=\'https://pl.wordpress.org/2018/02/06/wordpress-4-9-4-wydanie-poprawiajace-wazny-blad/\'>WordPress 4.9.4 – Wydanie poprawiające ważny błąd</a></li></ul></div><div class=\"rss-widget\"><ul><li><a class=\'rsswidget\' href=\'https://pl.wordpress.org/2018/02/06/wordpress-4-9-4-wydanie-poprawiajace-wazny-blad/\'>WordPress 4.9.4 – Wydanie poprawiające ważny błąd</a></li><li><a class=\'rsswidget\' href=\'https://pl.wordpress.org/2018/02/06/wordpress-4-9-3-wydanie-poprawiajace-bledy/\'>WordPress 4.9.3 – Wydanie poprawiające błędy</a></li><li><a class=\'rsswidget\' href=\'https://pl.wordpress.org/2018/01/17/wordpress-4-9-2-wydanie-poprawiajace-bezpieczenstwo-i-bledy/\'>WordPress 4.9.2 – Wydanie poprawiające bezpieczeństwo i błędy</a></li></ul></div>', 'no'),
(474, '_transient_timeout_wc_low_stock_count', '1527721351', 'no'),
(475, '_transient_wc_low_stock_count', '0', 'no'),
(476, '_transient_timeout_wc_outofstock_count', '1527721351', 'no'),
(477, '_transient_wc_outofstock_count', '0', 'no'),
(479, '_transient_is_multi_author', '0', 'yes');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_postmeta`
--

CREATE TABLE `rs_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `rs_postmeta`
--

INSERT INTO `rs_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 4, '_edit_last', '1'),
(3, 4, '_edit_lock', '1525093135:1'),
(4, 6, '_edit_last', '1'),
(5, 6, 'field_5ae6f5bd41e61', 'a:11:{s:3:\"key\";s:19:\"field_5ae6f5bd41e61\";s:5:\"label\";s:4:\"Logo\";s:4:\"name\";s:4:\"logo\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:2:\"id\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(6, 6, 'field_5ae6f5e641e62', 'a:11:{s:3:\"key\";s:19:\"field_5ae6f5e641e62\";s:5:\"label\";s:7:\"Address\";s:4:\"name\";s:6:\"adress\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:3:\"yes\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(7, 6, 'field_5ae6f61641e63', 'a:12:{s:3:\"key\";s:19:\"field_5ae6f61641e63\";s:5:\"label\";s:3:\"Map\";s:4:\"name\";s:3:\"map\";s:4:\"type\";s:10:\"google_map\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:10:\"center_lat\";s:0:\"\";s:10:\"center_lng\";s:0:\"\";s:4:\"zoom\";s:0:\"\";s:6:\"height\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}'),
(8, 6, 'field_5ae6f62641e64', 'a:14:{s:3:\"key\";s:19:\"field_5ae6f62641e64\";s:5:\"label\";s:0:\"\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}'),
(10, 6, 'position', 'acf_after_title'),
(11, 6, 'layout', 'default'),
(12, 6, 'hide_on_screen', 'a:1:{i:0;s:14:\"featured_image\";}'),
(13, 6, '_edit_lock', '1525090153:1'),
(15, 6, 'rule', 'a:5:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"4\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(16, 7, '_wp_attached_file', '2018/04/chatIcon.png'),
(17, 7, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:60;s:6:\"height\";i:53;s:4:\"file\";s:20:\"2018/04/chatIcon.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(18, 8, 'logo', '7'),
(19, 8, '_logo', 'field_5ae6f5bd41e61'),
(20, 8, 'adress', 'Solec 28/5\r\n\r\n00-403 Warszawa'),
(21, 8, '_adress', 'field_5ae6f5e641e62'),
(22, 8, 'map', ''),
(23, 8, '_map', 'field_5ae6f61641e63'),
(24, 8, '_', 'field_5ae6f62641e64'),
(25, 4, 'logo', '7'),
(26, 4, '_logo', 'field_5ae6f5bd41e61'),
(27, 4, 'adress', '<p style=\"text-align: left;\">Solec 28/5</p>\r\n<p style=\"text-align: left;\">00-403 Warszawa</p>'),
(28, 4, '_adress', 'field_5ae6f5e641e62'),
(29, 4, 'map', 'a:3:{s:7:\"address\";s:16:\"Warszawa, Polska\";s:3:\"lat\";s:10:\"52.2296756\";s:3:\"lng\";s:18:\"21.012228700000037\";}'),
(30, 4, '_map', 'field_5ae6f61641e63'),
(31, 4, '_', 'field_5ae6f62641e64'),
(39, 10, 'logo', '7'),
(40, 10, '_logo', 'field_5ae6f5bd41e61'),
(41, 10, 'adress', 'Solec 28/5\r\n\r\n00-403 Warszawa'),
(42, 10, '_adress', 'field_5ae6f5e641e62'),
(43, 10, 'map', ''),
(44, 10, '_map', 'field_5ae6f61641e63'),
(45, 10, '_', 'field_5ae6f62641e64'),
(46, 11, 'logo', '7'),
(47, 11, '_logo', 'field_5ae6f5bd41e61'),
(48, 11, 'adress', 'Solec 28/5\r\n\r\n00-403 Warszawa'),
(49, 11, '_adress', 'field_5ae6f5e641e62'),
(50, 11, 'map', 'a:3:{s:7:\"address\";s:16:\"Warszawa, Polska\";s:3:\"lat\";s:10:\"52.2296756\";s:3:\"lng\";s:18:\"21.012228700000037\";}'),
(51, 11, '_map', 'field_5ae6f61641e63'),
(52, 11, '_', 'field_5ae6f62641e64'),
(60, 13, 'logo', '7'),
(61, 13, '_logo', 'field_5ae6f5bd41e61'),
(62, 13, 'adress', 'Solec 28/5\r\n\r\n00-403 Warszawa'),
(63, 13, '_adress', 'field_5ae6f5e641e62'),
(64, 13, 'map', 'a:3:{s:7:\"address\";s:16:\"Warszawa, Polska\";s:3:\"lat\";s:10:\"52.2296756\";s:3:\"lng\";s:18:\"21.012228700000037\";}'),
(65, 13, '_map', 'field_5ae6f61641e63'),
(66, 13, '_', 'field_5ae6f62641e64'),
(67, 14, 'logo', '7'),
(68, 14, '_logo', 'field_5ae6f5bd41e61'),
(69, 14, 'adress', 'Solec 28/5\r\n\r\n00-403 Warszawa'),
(70, 14, '_adress', 'field_5ae6f5e641e62'),
(71, 14, 'map', 'a:3:{s:7:\"address\";s:16:\"Warszawa, Polska\";s:3:\"lat\";s:10:\"52.2296756\";s:3:\"lng\";s:18:\"21.012228700000037\";}'),
(72, 14, '_map', 'field_5ae6f61641e63'),
(73, 14, '_', 'field_5ae6f62641e64'),
(74, 15, 'logo', '7'),
(75, 15, '_logo', 'field_5ae6f5bd41e61'),
(76, 15, 'adress', '<p style=\"text-align: left;\">Solec 28/5</p>\r\n<p style=\"text-align: left;\">00-403 Warszawa</p>'),
(77, 15, '_adress', 'field_5ae6f5e641e62'),
(78, 15, 'map', 'a:3:{s:7:\"address\";s:16:\"Warszawa, Polska\";s:3:\"lat\";s:10:\"52.2296756\";s:3:\"lng\";s:18:\"21.012228700000037\";}'),
(79, 15, '_map', 'field_5ae6f61641e63'),
(80, 15, '_', 'field_5ae6f62641e64'),
(81, 16, '_edit_last', '1'),
(82, 16, 'field_5ae719abdd0e0', 'a:11:{s:3:\"key\";s:19:\"field_5ae719abdd0e0\";s:5:\"label\";s:5:\"Image\";s:4:\"name\";s:5:\"image\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:2:\"id\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(83, 16, 'field_5ae719c8dd0e1', 'a:14:{s:3:\"key\";s:19:\"field_5ae719c8dd0e1\";s:5:\"label\";s:4:\"Name\";s:4:\"name\";s:4:\"name\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(84, 16, 'field_5ae719eadd0e2', 'a:12:{s:3:\"key\";s:19:\"field_5ae719eadd0e2\";s:5:\"label\";s:5:\"Email\";s:4:\"name\";s:5:\"email\";s:4:\"type\";s:5:\"email\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}'),
(85, 16, 'field_5ae71a07dd0e3', 'a:14:{s:3:\"key\";s:19:\"field_5ae71a07dd0e3\";s:5:\"label\";s:3:\"WWW\";s:4:\"name\";s:3:\"www\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}'),
(86, 16, 'field_5ae71a39dd0e4', 'a:14:{s:3:\"key\";s:19:\"field_5ae71a39dd0e4\";s:5:\"label\";s:14:\"Social 1 Label\";s:4:\"name\";s:12:\"social1label\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:4;}'),
(87, 16, 'field_5ae71c18dd0e5', 'a:14:{s:3:\"key\";s:19:\"field_5ae71c18dd0e5\";s:5:\"label\";s:12:\"Social 1 URL\";s:4:\"name\";s:10:\"social1url\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:5;}'),
(88, 16, 'field_5ae71c30dd0e6', 'a:14:{s:3:\"key\";s:19:\"field_5ae71c30dd0e6\";s:5:\"label\";s:14:\"Social 2 Label\";s:4:\"name\";s:12:\"social2label\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:6;}'),
(89, 16, 'field_5ae71c3cdd0e7', 'a:14:{s:3:\"key\";s:19:\"field_5ae71c3cdd0e7\";s:5:\"label\";s:12:\"Social 2 URL\";s:4:\"name\";s:10:\"social2url\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:7;}'),
(90, 16, 'field_5ae71c5add0e8', 'a:14:{s:3:\"key\";s:19:\"field_5ae71c5add0e8\";s:5:\"label\";s:14:\"Social 3 Label\";s:4:\"name\";s:12:\"social3label\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:8;}'),
(91, 16, 'field_5ae71c64dd0e9', 'a:14:{s:3:\"key\";s:19:\"field_5ae71c64dd0e9\";s:5:\"label\";s:12:\"Social 3 URL\";s:4:\"name\";s:10:\"social3url\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:9;}'),
(92, 16, 'field_5ae71c7cdd0ea', 'a:14:{s:3:\"key\";s:19:\"field_5ae71c7cdd0ea\";s:5:\"label\";s:13:\"Related posts\";s:4:\"name\";s:13:\"related_posts\";s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"return_format\";s:6:\"object\";s:9:\"post_type\";a:1:{i:0;s:4:\"post\";}s:8:\"taxonomy\";a:1:{i:0;s:3:\"all\";}s:7:\"filters\";a:1:{i:0;s:6:\"search\";}s:15:\"result_elements\";a:1:{i:0;s:9:\"post_type\";}s:3:\"max\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:10;}'),
(93, 16, 'field_5ae71cc3dd0eb', 'a:14:{s:3:\"key\";s:19:\"field_5ae71cc3dd0eb\";s:5:\"label\";s:0:\"\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:11;}'),
(95, 16, 'position', 'normal'),
(96, 16, 'layout', 'no_box'),
(97, 16, 'hide_on_screen', 'a:1:{i:0;s:14:\"featured_image\";}'),
(98, 16, '_edit_lock', '1525114206:1'),
(99, 17, '_edit_last', '1'),
(100, 17, '_edit_lock', '1525119506:1'),
(101, 17, 'image', '7'),
(102, 17, '_image', 'field_5ae719abdd0e0'),
(103, 17, 'name', 'Pawel Struminski'),
(104, 17, '_name', 'field_5ae719c8dd0e1'),
(105, 17, 'email', 'pstruminski@gmail.com'),
(106, 17, '_email', 'field_5ae719eadd0e2'),
(107, 17, 'www', 'https://www.google.pl/'),
(108, 17, '_www', 'field_5ae71a07dd0e3'),
(109, 17, 'social1label', 'Facebook'),
(110, 17, '_social1label', 'field_5ae71a39dd0e4'),
(111, 17, 'social1url', 'https://www.facebook.com/'),
(112, 17, '_social1url', 'field_5ae71c18dd0e5'),
(113, 17, 'social2label', 'Instagram'),
(114, 17, '_social2label', 'field_5ae71c30dd0e6'),
(115, 17, 'social2url', 'https://www.instagram.com'),
(116, 17, '_social2url', 'field_5ae71c3cdd0e7'),
(117, 17, 'social3label', 'LinkedIn'),
(118, 17, '_social3label', 'field_5ae71c5add0e8'),
(119, 17, 'social_3_url', 'https://www.linkedin.com'),
(120, 17, '_social_3_url', 'field_5ae71c64dd0e9'),
(121, 17, '_', 'field_5ae71cc3dd0eb'),
(122, 17, 'related_posts', 'a:2:{i:0;s:1:\"1\";i:1;s:2:\"19\";}'),
(123, 17, '_related_posts', 'field_5ae71c7cdd0ea'),
(125, 17, 'social3url', 'https://www.linkedin.com'),
(126, 17, '_social3url', 'field_5ae71c64dd0e9'),
(129, 16, 'rule', 'a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"employee\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(130, 1, '_edit_lock', '1525118299:1'),
(131, 1, '_edit_last', '1'),
(132, 1, '_thumbnail_id', '7'),
(137, 19, '_edit_last', '1'),
(138, 19, '_edit_lock', '1525119471:1'),
(139, 20, '_wp_attached_file', '2018/04/infoIcon.png'),
(140, 20, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:68;s:6:\"height\";i:68;s:4:\"file\";s:20:\"2018/04/infoIcon.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(141, 19, '_thumbnail_id', '20'),
(144, 22, '_wc_review_count', '0'),
(145, 22, '_wc_rating_count', 'a:0:{}'),
(146, 22, '_wc_average_rating', '0'),
(147, 22, '_edit_last', '1'),
(148, 22, '_edit_lock', '1525127283:1'),
(149, 23, '_wp_attached_file', '2018/04/timer.png'),
(150, 23, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:56;s:6:\"height\";i:60;s:4:\"file\";s:17:\"2018/04/timer.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(151, 22, '_thumbnail_id', '23'),
(152, 22, '_sku', ''),
(153, 22, '_regular_price', '156'),
(154, 22, '_sale_price', '140'),
(155, 22, '_sale_price_dates_from', ''),
(156, 22, '_sale_price_dates_to', ''),
(157, 22, 'total_sales', '0'),
(158, 22, '_tax_status', 'taxable'),
(159, 22, '_tax_class', ''),
(160, 22, '_manage_stock', 'no'),
(161, 22, '_backorders', 'no'),
(162, 22, '_sold_individually', 'no'),
(163, 22, '_weight', ''),
(164, 22, '_length', ''),
(165, 22, '_width', ''),
(166, 22, '_height', ''),
(167, 22, '_upsell_ids', 'a:0:{}'),
(168, 22, '_crosssell_ids', 'a:0:{}'),
(169, 22, '_purchase_note', ''),
(170, 22, '_default_attributes', 'a:0:{}'),
(171, 22, '_virtual', 'no'),
(172, 22, '_downloadable', 'no'),
(173, 22, '_product_image_gallery', ''),
(174, 22, '_download_limit', '-1'),
(175, 22, '_download_expiry', '-1'),
(176, 22, '_stock', NULL),
(177, 22, '_stock_status', 'instock'),
(178, 22, '_product_version', '3.3.5'),
(179, 22, '_price', '140'),
(180, 27, '_wp_trash_meta_status', 'publish'),
(181, 27, '_wp_trash_meta_time', '1525124068'),
(182, 27, '_wp_desired_post_slug', 'cart'),
(183, 28, '_wp_trash_meta_status', 'publish'),
(184, 28, '_wp_trash_meta_time', '1525124070'),
(185, 28, '_wp_desired_post_slug', 'checkout'),
(186, 29, '_wp_trash_meta_status', 'publish'),
(187, 29, '_wp_trash_meta_time', '1525124074'),
(188, 29, '_wp_desired_post_slug', 'my-account'),
(189, 26, '_wp_trash_meta_status', 'publish'),
(190, 26, '_wp_trash_meta_time', '1525124075'),
(191, 26, '_wp_desired_post_slug', 'shop'),
(192, 34, '_edit_last', '1'),
(193, 34, 'field_5ae78eddfe6bb', 'a:14:{s:3:\"key\";s:19:\"field_5ae78eddfe6bb\";s:5:\"label\";s:13:\"Special Label\";s:4:\"name\";s:13:\"special_label\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(194, 34, 'field_5ae78eedfe6bc', 'a:14:{s:3:\"key\";s:19:\"field_5ae78eedfe6bc\";s:5:\"label\";s:0:\"\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(195, 34, 'rule', 'a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"product\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(196, 34, 'position', 'normal'),
(197, 34, 'layout', 'no_box'),
(198, 34, 'hide_on_screen', ''),
(199, 34, '_edit_lock', '1525127205:1'),
(200, 22, 'special_label', 'Special Label'),
(201, 22, '_special_label', 'field_5ae78eddfe6bb'),
(202, 22, '_', 'field_5ae78eedfe6bc'),
(203, 2, '_edit_lock', '1525126503:1'),
(204, 35, '_edit_last', '1'),
(205, 35, '_edit_lock', '1525134618:1'),
(206, 35, 'image', '23'),
(207, 35, '_image', 'field_5ae719abdd0e0'),
(208, 35, 'name', 'Jan Kowalski'),
(209, 35, '_name', 'field_5ae719c8dd0e1'),
(210, 35, 'email', 'jan@jan.pl'),
(211, 35, '_email', 'field_5ae719eadd0e2'),
(212, 35, 'www', 'www.jan.pl'),
(213, 35, '_www', 'field_5ae71a07dd0e3'),
(214, 35, 'social1label', ''),
(215, 35, '_social1label', 'field_5ae71a39dd0e4'),
(216, 35, 'social1url', ''),
(217, 35, '_social1url', 'field_5ae71c18dd0e5'),
(218, 35, 'social2label', ''),
(219, 35, '_social2label', 'field_5ae71c30dd0e6'),
(220, 35, 'social2url', ''),
(221, 35, '_social2url', 'field_5ae71c3cdd0e7'),
(222, 35, 'social3label', ''),
(223, 35, '_social3label', 'field_5ae71c5add0e8'),
(224, 35, 'social3url', ''),
(225, 35, '_social3url', 'field_5ae71c64dd0e9'),
(226, 35, 'related_posts', ''),
(227, 35, '_related_posts', 'field_5ae71c7cdd0ea'),
(228, 35, '_', 'field_5ae71cc3dd0eb');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_posts`
--

CREATE TABLE `rs_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `rs_posts`
--

INSERT INTO `rs_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-04-30 11:51:27', '2018-04-30 09:51:27', 'Witaj w WordPressie. To jest twój pierwszy wpis. Zmodyfikuj go lub usuń, a następnie rozpocznij pisanie!', 'Witaj, świecie!', '', 'publish', 'open', 'open', '', 'witaj-swiecie', '', '', '2018-04-30 22:00:39', '2018-04-30 20:00:39', '', 0, 'http://localhost/RisenTestTask/?p=1', 0, 'post', '', 1),
(2, 1, '2018-04-30 11:51:27', '2018-04-30 09:51:27', 'To jest przykładowa strona. Strony są inne niż wpisy na blogu, ponieważ nie tylko znajdują się zawsze w jednym miejscu, ale także pojawiają się w menu witryny (w większości motywów). Większość użytkowników umieszcza na swoich witrynach stronę z informacjami o sobie, dzięki którym przedstawiają się odwiedzającym ich witrynę. Taka strona może zawierać na przykład taki tekst:\n\n<blockquote>Cześć! Za dnia jestem gońcem, a nocą próbuję swoich sił w aktorstwie. To jest moja witryna. Mieszkam w Los Angeles, mam wspaniałego psa, który wabi się Jack i lubię pi&#241;a coladę (a także spacery, gdy pada deszcz).</blockquote>\n\n... lub taki:\n\n<blockquote>Firma Wihajstry XYZ została założona w 1971 roku i od początku swojego istnienia zajmuje się produkcją najlepszych wihajstrów. W naszej siedzibie w Gotham City pracuje ponad 2000 osób, które zajmują się robieniem całej masy fantastycznych rzeczy dla mieszkańców Gotham.</blockquote>\n\nJako nowy użytkownik WordPressa powinieneś przejść do <a href=\"http://localhost/RisenTestTask/wp-admin/\">swojego kokpitu</a>, aby usunąć tę stronę i utworzyć nowe strony z własną treścią. Baw się dobrze!', 'Przykładowa strona', '', 'publish', 'closed', 'open', '', 'przykladowa-strona', '', '', '2018-04-30 11:51:27', '2018-04-30 09:51:27', '', 0, 'http://localhost/RisenTestTask/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-04-30 11:51:49', '0000-00-00 00:00:00', '', 'Automatycznie zapisany szkic', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-04-30 11:51:49', '0000-00-00 00:00:00', '', 0, 'http://localhost/RisenTestTask/?p=3', 0, 'post', '', 0),
(4, 1, '2018-04-30 12:50:57', '2018-04-30 10:50:57', '<h3 style=\"text-align: center;\">Strona Kontakt</h3>\r\n<h3 style=\"text-align: center;\">Adres i Mapa</h3>', 'Contact', '', 'publish', 'closed', 'closed', '', 'contact', '', '', '2018-04-30 15:01:14', '2018-04-30 13:01:14', '', 0, 'http://localhost/RisenTestTask/?page_id=4', 0, 'page', '', 0),
(5, 1, '2018-04-30 12:50:57', '2018-04-30 10:50:57', '', 'Contact', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2018-04-30 12:50:57', '2018-04-30 10:50:57', '', 4, 'http://localhost/RisenTestTask/index.php/2018/04/30/4-revision-v1/', 0, 'revision', '', 0),
(6, 1, '2018-04-30 12:55:56', '2018-04-30 10:55:56', '', 'Contact', '', 'publish', 'closed', 'closed', '', 'acf_contact', '', '', '2018-04-30 12:59:19', '2018-04-30 10:59:19', '', 0, 'http://localhost/RisenTestTask/?post_type=acf&#038;p=6', 0, 'acf', '', 0),
(7, 1, '2018-04-30 13:16:18', '2018-04-30 11:16:18', '', 'chatIcon', '', 'inherit', 'open', 'closed', '', 'chaticon', '', '', '2018-04-30 13:16:18', '2018-04-30 11:16:18', '', 4, 'http://localhost/RisenTestTask/wp-content/uploads/2018/04/chatIcon.png', 0, 'attachment', 'image/png', 0),
(8, 1, '2018-04-30 13:16:42', '2018-04-30 11:16:42', '', 'Contact', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2018-04-30 13:16:42', '2018-04-30 11:16:42', '', 4, 'http://localhost/RisenTestTask/index.php/2018/04/30/4-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2018-04-30 13:44:18', '2018-04-30 11:44:18', '', 'Contact', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2018-04-30 13:44:18', '2018-04-30 11:44:18', '', 4, 'http://localhost/RisenTestTask/index.php/2018/04/30/4-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2018-04-30 13:44:38', '2018-04-30 11:44:38', '', 'Contact', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2018-04-30 13:44:38', '2018-04-30 11:44:38', '', 4, 'http://localhost/RisenTestTask/index.php/2018/04/30/4-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2018-04-30 14:52:53', '2018-04-30 12:52:53', 'Strona Kontakt\r\n\r\nAdres i Mapa', 'Contact', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2018-04-30 14:52:53', '2018-04-30 12:52:53', '', 4, 'http://localhost/RisenTestTask/index.php/2018/04/30/4-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2018-04-30 14:53:10', '2018-04-30 12:53:10', 'Strona Kontakt\r\n\r\nAdres i Mapa', 'Contact', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2018-04-30 14:53:10', '2018-04-30 12:53:10', '', 4, 'http://localhost/RisenTestTask/index.php/2018/04/30/4-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2018-04-30 15:01:14', '2018-04-30 13:01:14', '<h3 style=\"text-align: center;\">Strona Kontakt</h3>\r\n<h3 style=\"text-align: center;\">Adres i Mapa</h3>', 'Contact', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2018-04-30 15:01:14', '2018-04-30 13:01:14', '', 4, 'http://localhost/RisenTestTask/index.php/2018/04/30/4-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2018-04-30 15:41:09', '2018-04-30 13:41:09', '', 'Employee', '', 'publish', 'closed', 'closed', '', 'acf_employee', '', '', '2018-04-30 20:52:22', '2018-04-30 18:52:22', '', 0, 'http://localhost/RisenTestTask/?post_type=acf&#038;p=16', 0, 'acf', '', 0),
(17, 1, '2018-04-30 15:44:35', '2018-04-30 13:44:35', 'Short description here', 'Pawel Struminski', '', 'publish', 'closed', 'closed', '', 'pawel-struminski', '', '', '2018-04-30 22:20:47', '2018-04-30 20:20:47', '', 0, 'http://localhost/RisenTestTask/?post_type=employee&#038;p=17', 0, 'employee', '', 0),
(18, 1, '2018-04-30 22:00:16', '2018-04-30 20:00:16', 'Witaj w WordPressie. To jest twój pierwszy wpis. Zmodyfikuj go lub usuń, a następnie rozpocznij pisanie!', 'Witaj, świecie!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-04-30 22:00:16', '2018-04-30 20:00:16', '', 1, 'http://localhost/RisenTestTask/?p=18', 0, 'revision', '', 0),
(19, 1, '2018-04-30 22:20:11', '2018-04-30 20:20:11', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"', 'Test', '', 'publish', 'open', 'open', '', 'test', '', '', '2018-04-30 22:20:11', '2018-04-30 20:20:11', '', 0, 'http://localhost/RisenTestTask/?p=19', 0, 'post', '', 0),
(20, 1, '2018-04-30 22:20:05', '2018-04-30 20:20:05', '', 'infoIcon', '', 'inherit', 'open', 'closed', '', 'infoicon', '', '', '2018-04-30 22:20:05', '2018-04-30 20:20:05', '', 19, 'http://localhost/RisenTestTask/wp-content/uploads/2018/04/infoIcon.png', 0, 'attachment', 'image/png', 0),
(21, 1, '2018-04-30 22:20:11', '2018-04-30 20:20:11', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"', 'Test', '', 'inherit', 'closed', 'closed', '', '19-revision-v1', '', '', '2018-04-30 22:20:11', '2018-04-30 20:20:11', '', 19, 'http://localhost/RisenTestTask/?p=21', 0, 'revision', '', 0),
(22, 1, '2018-04-30 22:35:29', '2018-04-30 20:35:29', 'Wspaniały produkt testowy', 'Test Product', 'Super product', 'publish', 'open', 'closed', '', 'test-product', '', '', '2018-05-01 00:30:21', '2018-04-30 22:30:21', '', 0, 'http://localhost/RisenTestTask/?post_type=product&#038;p=22', 0, 'product', '', 0),
(23, 1, '2018-04-30 22:35:22', '2018-04-30 20:35:22', '', 'timer', '', 'inherit', 'open', 'closed', '', 'timer', '', '', '2018-04-30 22:35:22', '2018-04-30 20:35:22', '', 22, 'http://localhost/RisenTestTask/wp-content/uploads/2018/04/timer.png', 0, 'attachment', 'image/png', 0),
(24, 1, '2018-04-30 23:18:42', '0000-00-00 00:00:00', '', 'Automatycznie zapisany szkic', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-04-30 23:18:42', '0000-00-00 00:00:00', '', 0, 'http://localhost/RisenTestTask/?post_type=acf&p=24', 0, 'acf', '', 0),
(25, 1, '2018-04-30 23:21:23', '0000-00-00 00:00:00', '', 'Automatycznie zapisany szkic', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-04-30 23:21:23', '0000-00-00 00:00:00', '', 0, 'http://localhost/RisenTestTask/?page_id=25', 0, 'page', '', 0),
(26, 1, '2018-04-30 23:21:49', '2018-04-30 21:21:49', '', 'Shop', '', 'trash', 'closed', 'closed', '', 'shop__trashed', '', '', '2018-04-30 23:34:35', '2018-04-30 21:34:35', '', 0, 'http://localhost/RisenTestTask/?page_id=26', 0, 'page', '', 0),
(27, 1, '2018-04-30 23:21:49', '2018-04-30 21:21:49', '[woocommerce_cart]', 'Cart', '', 'trash', 'closed', 'closed', '', 'cart__trashed', '', '', '2018-04-30 23:34:28', '2018-04-30 21:34:28', '', 0, 'http://localhost/RisenTestTask/?page_id=27', 0, 'page', '', 0),
(28, 1, '2018-04-30 23:21:49', '2018-04-30 21:21:49', '[woocommerce_checkout]', 'Checkout', '', 'trash', 'closed', 'closed', '', 'checkout__trashed', '', '', '2018-04-30 23:34:30', '2018-04-30 21:34:30', '', 0, 'http://localhost/RisenTestTask/?page_id=28', 0, 'page', '', 0),
(29, 1, '2018-04-30 23:21:49', '2018-04-30 21:21:49', '[woocommerce_my_account]', 'My account', '', 'trash', 'closed', 'closed', '', 'my-account__trashed', '', '', '2018-04-30 23:34:34', '2018-04-30 21:34:34', '', 0, 'http://localhost/RisenTestTask/?page_id=29', 0, 'page', '', 0),
(30, 1, '2018-04-30 23:34:28', '2018-04-30 21:34:28', '[woocommerce_cart]', 'Cart', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2018-04-30 23:34:28', '2018-04-30 21:34:28', '', 27, 'http://localhost/RisenTestTask/?p=30', 0, 'revision', '', 0),
(31, 1, '2018-04-30 23:34:30', '2018-04-30 21:34:30', '[woocommerce_checkout]', 'Checkout', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2018-04-30 23:34:30', '2018-04-30 21:34:30', '', 28, 'http://localhost/RisenTestTask/?p=31', 0, 'revision', '', 0),
(32, 1, '2018-04-30 23:34:34', '2018-04-30 21:34:34', '[woocommerce_my_account]', 'My account', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2018-04-30 23:34:34', '2018-04-30 21:34:34', '', 29, 'http://localhost/RisenTestTask/?p=32', 0, 'revision', '', 0),
(33, 1, '2018-04-30 23:34:35', '2018-04-30 21:34:35', '', 'Shop', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2018-04-30 23:34:35', '2018-04-30 21:34:35', '', 26, 'http://localhost/RisenTestTask/?p=33', 0, 'revision', '', 0),
(34, 1, '2018-04-30 23:50:54', '2018-04-30 21:50:54', '', 'Woocomerce', '', 'publish', 'closed', 'closed', '', 'acf_woocomerce', '', '', '2018-04-30 23:50:54', '2018-04-30 21:50:54', '', 0, 'http://localhost/RisenTestTask/?post_type=acf&#038;p=34', 0, 'acf', '', 0),
(35, 1, '2018-05-01 02:32:36', '2018-05-01 00:32:36', '', 'Jan Kowalski', '', 'publish', 'closed', 'closed', '', 'jan-kowalski', '', '', '2018-05-01 02:32:36', '2018-05-01 00:32:36', '', 0, 'http://localhost/RisenTestTask/?post_type=employee&#038;p=35', 0, 'employee', '', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_termmeta`
--

CREATE TABLE `rs_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `rs_termmeta`
--

INSERT INTO `rs_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 15, 'product_count_product_cat', '1');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_terms`
--

CREATE TABLE `rs_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `rs_terms`
--

INSERT INTO `rs_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Bez kategorii', 'bez-kategorii', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'Uncategorized', 'uncategorized', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_term_relationships`
--

CREATE TABLE `rs_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `rs_term_relationships`
--

INSERT INTO `rs_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(19, 1, 0),
(22, 2, 0),
(22, 15, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_term_taxonomy`
--

CREATE TABLE `rs_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `rs_term_taxonomy`
--

INSERT INTO `rs_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 2),
(2, 2, 'product_type', '', 0, 1),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 0),
(7, 7, 'product_visibility', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_usermeta`
--

CREATE TABLE `rs_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `rs_usermeta`
--

INSERT INTO `rs_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'risen'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'rs_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'rs_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:2:{s:64:\"528f35daddc7345bae5e7486ac2593e28a15622231831ad23b37046b5ffd5970\";a:4:{s:10:\"expiration\";i:1525254708;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:117:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1 Safari/605.1.15\";s:5:\"login\";i:1525081908;}s:64:\"18453c01f3f60091f17988245602c4bf6c4e2c783444df42599ec9f880b32f38\";a:4:{s:10:\"expiration\";i:1525256787;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36\";s:5:\"login\";i:1525083987;}}'),
(17, 1, 'rs_dashboard_quick_press_last_post_id', '3'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:2:\"::\";}'),
(19, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:1:{s:32:\"b6d767d2f8ed5d21a44b0e5886680cb9\";a:10:{s:3:\"key\";s:32:\"b6d767d2f8ed5d21a44b0e5886680cb9\";s:10:\"product_id\";i:22;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:140;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:140;s:8:\"line_tax\";i:0;}}}'),
(20, 1, 'rs_user-settings', 'libraryContent=browse'),
(21, 1, 'rs_user-settings-time', '1525086997');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_users`
--

CREATE TABLE `rs_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `rs_users`
--

INSERT INTO `rs_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'risen', '$P$B22JF/W3OoQbb/Ce8pqt01vIZZNiS6/', 'risen', 'pstruminski@gmail.com', '', '2018-04-30 09:51:27', '', 0, 'risen');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_wc_download_log`
--

CREATE TABLE `rs_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_wc_webhooks`
--

CREATE TABLE `rs_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_woocommerce_api_keys`
--

CREATE TABLE `rs_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_520_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_woocommerce_attribute_taxonomies`
--

CREATE TABLE `rs_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `rs_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_woocommerce_log`
--

CREATE TABLE `rs_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_woocommerce_order_itemmeta`
--

CREATE TABLE `rs_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_woocommerce_order_items`
--

CREATE TABLE `rs_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_woocommerce_payment_tokenmeta`
--

CREATE TABLE `rs_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_woocommerce_payment_tokens`
--

CREATE TABLE `rs_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_woocommerce_sessions`
--

CREATE TABLE `rs_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `rs_woocommerce_sessions`
--

INSERT INTO `rs_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(4, '1', 'a:13:{s:4:\"cart\";s:356:\"a:1:{s:32:\"b6d767d2f8ed5d21a44b0e5886680cb9\";a:10:{s:3:\"key\";s:32:\"b6d767d2f8ed5d21a44b0e5886680cb9\";s:10:\"product_id\";i:22;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:140;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:140;s:8:\"line_tax\";i:0;}}\";s:11:\"cart_totals\";s:408:\"a:15:{s:8:\"subtotal\";s:6:\"140.00\";s:12:\"subtotal_tax\";d:0;s:14:\"shipping_total\";s:4:\"1.00\";s:12:\"shipping_tax\";d:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";d:0;s:12:\"discount_tax\";d:0;s:19:\"cart_contents_total\";s:6:\"140.00\";s:17:\"cart_contents_tax\";d:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";s:4:\"0.00\";s:7:\"fee_tax\";d:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";s:6:\"141.00\";s:9:\"total_tax\";d:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:6:\"a:0:{}\";s:8:\"customer\";s:709:\"a:26:{s:2:\"id\";s:1:\"1\";s:13:\"date_modified\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:0:\"\";s:7:\"country\";s:2:\"GB\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:0:\"\";s:16:\"shipping_country\";s:2:\"GB\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:21:\"pstruminski@gmail.com\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";s:10:\"wc_notices\";N;s:22:\"shipping_for_package_0\";s:378:\"a:2:{s:12:\"package_hash\";s:40:\"wc_ship_631234779f2302fef27fb0ad140c2d5f\";s:5:\"rates\";a:1:{s:11:\"flat_rate:1\";O:16:\"WC_Shipping_Rate\":2:{s:7:\"\0*\0data\";a:6:{s:2:\"id\";s:11:\"flat_rate:1\";s:9:\"method_id\";s:9:\"flat_rate\";s:11:\"instance_id\";i:1;s:5:\"label\";s:9:\"Flat rate\";s:4:\"cost\";s:4:\"1.00\";s:5:\"taxes\";a:0:{}}s:12:\"\0*\0meta_data\";a:1:{s:5:\"Items\";s:22:\"Test Product &times; 1\";}}}}\";s:25:\"previous_shipping_methods\";s:39:\"a:1:{i:0;a:1:{i:0;s:11:\"flat_rate:1\";}}\";s:23:\"chosen_shipping_methods\";s:29:\"a:1:{i:0;s:11:\"flat_rate:1\";}\";s:22:\"shipping_method_counts\";s:14:\"a:1:{i:0;i:1;}\";s:21:\"chosen_payment_method\";s:11:\"ppec_paypal\";}', 1525294462);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_woocommerce_shipping_zones`
--

CREATE TABLE `rs_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `rs_woocommerce_shipping_zones`
--

INSERT INTO `rs_woocommerce_shipping_zones` (`zone_id`, `zone_name`, `zone_order`) VALUES
(1, 'United Kingdom (UK)', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_woocommerce_shipping_zone_locations`
--

CREATE TABLE `rs_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `rs_woocommerce_shipping_zone_locations`
--

INSERT INTO `rs_woocommerce_shipping_zone_locations` (`location_id`, `zone_id`, `location_code`, `location_type`) VALUES
(1, 1, 'GB', 'country');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_woocommerce_shipping_zone_methods`
--

CREATE TABLE `rs_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Zrzut danych tabeli `rs_woocommerce_shipping_zone_methods`
--

INSERT INTO `rs_woocommerce_shipping_zone_methods` (`zone_id`, `instance_id`, `method_id`, `method_order`, `is_enabled`) VALUES
(1, 1, 'flat_rate', 1, 1),
(0, 2, 'flat_rate', 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_woocommerce_tax_rates`
--

CREATE TABLE `rs_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rs_woocommerce_tax_rate_locations`
--

CREATE TABLE `rs_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `rs_commentmeta`
--
ALTER TABLE `rs_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `rs_comments`
--
ALTER TABLE `rs_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `rs_links`
--
ALTER TABLE `rs_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `rs_options`
--
ALTER TABLE `rs_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `rs_postmeta`
--
ALTER TABLE `rs_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `rs_posts`
--
ALTER TABLE `rs_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `rs_termmeta`
--
ALTER TABLE `rs_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `rs_terms`
--
ALTER TABLE `rs_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `rs_term_relationships`
--
ALTER TABLE `rs_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `rs_term_taxonomy`
--
ALTER TABLE `rs_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `rs_usermeta`
--
ALTER TABLE `rs_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `rs_users`
--
ALTER TABLE `rs_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `rs_wc_download_log`
--
ALTER TABLE `rs_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `rs_wc_webhooks`
--
ALTER TABLE `rs_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `rs_woocommerce_api_keys`
--
ALTER TABLE `rs_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `rs_woocommerce_attribute_taxonomies`
--
ALTER TABLE `rs_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `rs_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `rs_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `rs_woocommerce_log`
--
ALTER TABLE `rs_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `rs_woocommerce_order_itemmeta`
--
ALTER TABLE `rs_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `rs_woocommerce_order_items`
--
ALTER TABLE `rs_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `rs_woocommerce_payment_tokenmeta`
--
ALTER TABLE `rs_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `rs_woocommerce_payment_tokens`
--
ALTER TABLE `rs_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `rs_woocommerce_sessions`
--
ALTER TABLE `rs_woocommerce_sessions`
  ADD PRIMARY KEY (`session_key`),
  ADD UNIQUE KEY `session_id` (`session_id`);

--
-- Indexes for table `rs_woocommerce_shipping_zones`
--
ALTER TABLE `rs_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `rs_woocommerce_shipping_zone_locations`
--
ALTER TABLE `rs_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `rs_woocommerce_shipping_zone_methods`
--
ALTER TABLE `rs_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `rs_woocommerce_tax_rates`
--
ALTER TABLE `rs_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `rs_woocommerce_tax_rate_locations`
--
ALTER TABLE `rs_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `rs_commentmeta`
--
ALTER TABLE `rs_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `rs_comments`
--
ALTER TABLE `rs_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `rs_links`
--
ALTER TABLE `rs_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `rs_options`
--
ALTER TABLE `rs_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=480;

--
-- AUTO_INCREMENT dla tabeli `rs_postmeta`
--
ALTER TABLE `rs_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=229;

--
-- AUTO_INCREMENT dla tabeli `rs_posts`
--
ALTER TABLE `rs_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT dla tabeli `rs_termmeta`
--
ALTER TABLE `rs_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `rs_terms`
--
ALTER TABLE `rs_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT dla tabeli `rs_term_taxonomy`
--
ALTER TABLE `rs_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT dla tabeli `rs_usermeta`
--
ALTER TABLE `rs_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT dla tabeli `rs_users`
--
ALTER TABLE `rs_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `rs_wc_download_log`
--
ALTER TABLE `rs_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `rs_wc_webhooks`
--
ALTER TABLE `rs_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `rs_woocommerce_api_keys`
--
ALTER TABLE `rs_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `rs_woocommerce_attribute_taxonomies`
--
ALTER TABLE `rs_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `rs_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `rs_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `rs_woocommerce_log`
--
ALTER TABLE `rs_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `rs_woocommerce_order_itemmeta`
--
ALTER TABLE `rs_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `rs_woocommerce_order_items`
--
ALTER TABLE `rs_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `rs_woocommerce_payment_tokenmeta`
--
ALTER TABLE `rs_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `rs_woocommerce_payment_tokens`
--
ALTER TABLE `rs_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `rs_woocommerce_sessions`
--
ALTER TABLE `rs_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `rs_woocommerce_shipping_zones`
--
ALTER TABLE `rs_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `rs_woocommerce_shipping_zone_locations`
--
ALTER TABLE `rs_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `rs_woocommerce_shipping_zone_methods`
--
ALTER TABLE `rs_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `rs_woocommerce_tax_rates`
--
ALTER TABLE `rs_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `rs_woocommerce_tax_rate_locations`
--
ALTER TABLE `rs_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
