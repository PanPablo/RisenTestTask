<?php
/**
 * Created by PhpStorm.
 * User: pawelstruminski
 * Date: 30.04.2018
 * Time: 15:46
 */

/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<div class="container">
    <?php if ( have_posts() ) : while ( have_posts() ) :    the_post(); ?>
      <!-- post -->
        <div class="row">
            <div class="col-md">
                <p><?php the_content(); ?></p>
                <div>
                <?php

                $image = get_field('image');
                $size = 'full'; // (thumbnail, medium, large, full or custom size)

                if( $image ) {

                    echo wp_get_attachment_image( $image, $size );

                }

                ?>
                </div>

                <p><?php the_field('name'); ?></p>
                <p><?php the_field('email'); ?></p>
                <p><?php the_field('www'); ?></p>
                <p><?php the_field('social1label'); ?>
                    <?php the_field('social1url'); ?></p>
                <p><?php the_field('social2label'); ?>
                    <?php the_field('social2url'); ?></p>
                <p><?php the_field('social3label'); ?>
                <?php the_field('social3url'); ?></p>
<!--                <div>--><?php //the_field('related_posts'); ?><!--<div>-->
            </div>
        </div>
    <?php endwhile; ?>
      <!-- post navigation -->
    <?php else: ?>
      <!-- no posts found -->
    <?php endif; ?>


<!--Related posts-->
    <p>Related Posts:</p>
    <?php

    $posts = get_field('related_posts');

    if( $posts ): ?>
        <ul>
            <?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>

                <li>
                    <div><?php echo get_the_post_thumbnail( $p->ID ); ?></div>
                    <a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a>
                    <p><?php echo implode(' ',array_slice(explode(' ',strip_tags($p->post_content)),0,40)).'...';?></p>                    <a href="<?php echo get_the_permalink( $p->ID ); ?>">Read More</a>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>

</div>

<?php get_footer(); ?>

