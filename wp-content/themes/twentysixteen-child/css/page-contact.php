<?php
/**
 * Created by PhpStorm.
 * User: pawelstruminski
 * Date: 30.04.2018
 * Time: 12:51
 */
get_header(); ?>
<div class="container">
    <div class="row">
        <div class="col-sm">
            <?php if ( have_posts() ) : while ( have_posts() ) :    the_post(); ?>
                <?php the_content() ?>
            <?php endwhile; ?>
                <!-- post navigation -->
            <?php else: ?>
                <!-- no posts found -->
            <?php endif; ?>

        </div>
    </div>


    <div class="row">

        <?php if ( have_posts() ) : while ( have_posts() ) :    the_post(); ?>
            <!-- post -->
            <div class="col-sm">
                <?php

                $image = get_field('logo');
                $size = 'full'; // (thumbnail, medium, large, full or custom size)

                if( $image ) {

                    echo wp_get_attachment_image( $image, $size );

                }

                ?>
                <p><?php the_field('adress') ?></p>

            </div>
        <?php endwhile; ?>
            <!-- post navigation -->
        <?php else: ?>
            <!-- no posts found -->
        <?php endif; ?>

        <div class="col-sm">

            <?php

            $location = get_field('map');

            if( !empty($location) ):
                ?>
                <div class="acf-map">
                    <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                </div>
            <?php endif; ?>

        </div>
    </div>
</div>


<?php get_footer(); ?>
