<?php

function Employee_init() {
	register_post_type( 'Employee', array(
		'labels'            => array(
			'name'                => __( 'Employees', 'em' ),
			'singular_name'       => __( 'Employee', 'em' ),
			'all_items'           => __( 'All Employees', 'em' ),
			'new_item'            => __( 'New employee', 'em' ),
			'add_new'             => __( 'Add New', 'em' ),
			'add_new_item'        => __( 'Add New employee', 'em' ),
			'edit_item'           => __( 'Edit employee', 'em' ),
			'view_item'           => __( 'View employee', 'em' ),
			'search_items'        => __( 'Search employees', 'em' ),
			'not_found'           => __( 'No employees found', 'em' ),
			'not_found_in_trash'  => __( 'No employees found in trash', 'em' ),
			'parent_item_colon'   => __( 'Parent employee', 'em' ),
			'menu_name'           => __( 'Employees', 'em' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
		'show_in_rest'      => true,
		'rest_base'         => 'Employee',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'Employee_init' );

function Employee_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['Employee'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Employee updated. <a target="_blank" href="%s">View employee</a>', 'em'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'em'),
		3 => __('Custom field deleted.', 'em'),
		4 => __('Employee updated.', 'em'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Employee restored to revision from %s', 'em'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Employee published. <a href="%s">View employee</a>', 'em'), esc_url( $permalink ) ),
		7 => __('Employee saved.', 'em'),
		8 => sprintf( __('Employee submitted. <a target="_blank" href="%s">Preview employee</a>', 'em'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Employee scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview employee</a>', 'em'),
		// translators: Publish box date format, see https://secure.php.net/manual/en/function.date.php
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Employee draft updated. <a target="_blank" href="%s">Preview employee</a>', 'em'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'Employee_updated_messages' );
