<?php
/**
 * Created by PhpStorm.
 * User: pawelstruminski
 * Date: 30.04.2018
 * Time: 20:12
 */
?>
<div class="container">
    <?php if ( have_posts() ) : while ( have_posts() ) :    the_post(); ?>
<!-- post -->
<div class="row">
    <div class="col-md">
        <div><?php the_content(); ?></div>
        <div>
            <?php

            $image = get_field('image');
            $size = 'full'; // (thumbnail, medium, large, full or custom size)

            if( $image ) {

                echo wp_get_attachment_image( $image, $size );

            }

            ?>
        </div>

        <p><?php the_field('name'); ?></p>
        <p><?php the_field('email'); ?></p>
        <p><?php the_field('www'); ?></p>
        <p><?php the_field('social1label'); ?>
            <?php the_field('social1url'); ?></p>
        <p><?php the_field('social2label'); ?>
            <?php the_field('social2url'); ?></p>
        <p><?php the_field('social3label'); ?>
            <?php the_field('social3url'); ?></p>
        <div><?php the_field('related_posts'); ?><div>
            </div>
        </div>
        <?php endwhile; ?>
        <!-- post navigation -->
        <?php else: ?>
            <!-- no posts found -->
        <?php endif; ?>

    </div>